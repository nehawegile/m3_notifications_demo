package com.m3.sdk.data.network

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NetworkHelper {

    class AuthCallback(val onSuccess: (AuthResponse?) -> Unit,
                       val onError: (Throwable?) -> Unit,
                       val onUserNotVerified: () -> Unit) : Callback<AuthResponse> {

        override fun onResponse(call: Call<AuthResponse>?, response: Response<AuthResponse>?) {
            if (response?.raw()?.code() == 403) {
                onUserNotVerified.invoke()
            }
            else {
                onSuccess.invoke(response?.body())
            }
            log("onResponse: Request = ${call?.request().toString()} Body = ${call?.request()?.body()} Response: ${response?.body().toString()}")
        }

        override fun onFailure(call: Call<AuthResponse>?, t: Throwable?) {
            onError.invoke(t)
        }
    }

    class NoCallback() : RetrofitCallback<Any>({}, {})

    open class RetrofitCallback <T> (val onSuccess: (response: T?) -> Unit, val onError: (error: Throwable?) -> Unit = {}) : Callback<T> {

        override fun onResponse(call: Call<T>?, response: Response<T>?) {
            onSuccess.invoke(response?.body())
            log("onResponse: Request = ${call?.request().toString()} Body = ${call?.request()?.body()} Response: ${response?.body().toString()}")
        }

        override fun onFailure(call: Call<T>?, t: Throwable?) {
            onError.invoke(t)
            log("onFailure: ${call?.toString()} Response: ${t?.toString()}")
        }
    }



    public data class AuthBody(var apiKey: String,
                        var apiSecret: String,
                        var uuid: String)

    data class AuthResponse(var success: Boolean,
                            var userId: String,
                            var token: String,
                            var application: Application,
                            var message: String) {

        data class Application(var senderId: String,
                               var packageName: String)
    }

    data class DeviceInfoBody(var deviceToken: String? = null,
                              var email: String? = null,
                              var phone: String? = null,
                              var name: String? = null,
                              var gender: String? = null,
                              var language: String? = null,
                              var country: String? = null,
                              var address: String? = null,
                              var state: String? = null,
                              var city: String? = null,
                              var zipcode: String? = null,
                              var osNumber: String? = null,
                              var deviceVendor: String? = null,
                              var deviceModel: String? = null,
                              var devicePlatform: String? = null,
                              var carrier: String? = null,
                              var connectivityType: String? = null,
                              var sdkVersion: String? = null,
                              var packageName: String? = null,
                              var appVersion: String? = null,
                              var pushNotificationEnabled: Boolean? = null,
                              var location: UserLocation? = null,
                              var age: Int? = null)

    class UserLocation(var lat: Double? = null, var lng: Double? = null)
}