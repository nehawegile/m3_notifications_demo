package com.m3.sdk.data.network

import android.util.Log
import com.m3.sdk.models.DynamicPushEvent
import com.m3.sdk.models.PushNotification
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.*

val isDebug = true

val tag = "AppTag"

fun log(any: Any?) {
    if (isDebug) {
        //Log.d(tag, "${any?.toString()}")
        System.out.println("$tag ${any?.toString()}")
    }
}

 interface ApiService {

    @POST("mobile/authenticate")
    fun auth(@Body body: NetworkHelper.AuthBody): Call<NetworkHelper.AuthResponse>

    @POST("mobile/user/{userId}/track_event")
    fun trackEvent(@Path("userId") userId: String,
                   @Query("mobileToken") token: String,
                   @Body event: DynamicPushEvent): Call<Any>

    @POST("mobile/user/{userId}/user_device")
    fun updateDeviceInfo(@Path("userId") userId: String,
                         @Query("mobileToken") token: String,
                         @Body info: NetworkHelper.DeviceInfoBody): Call<NetworkHelper.DeviceInfoBody>

    @GET("mobile/send_verification_code")
    fun sendVerificationCode(@Query("uuid") uuid: String,
                             @Query("verifyMethod") verifyMethod: String?,
                             @Query("email") email: String?,
                             @Query("phone") phone: String?): Call<Any>

    @GET("mobile/verify_code")
    fun verifyCode(@Query("uuid") uuid: String,
                   @Query("verifyToken") verifyToken: String): Call<Any>

    @GET("mobile/user/{appUserId}/unread_notifications")
    fun getUnreadNotifications(@Path("appUserId") userId: String,
                               @Query("mobileToken") token: String,
                               @Query("limit") limit: Double? = null,
                               @Query("offset") offset: Double? = null): Call<ArrayList<PushNotification>>

    @GET("mobile/user/{appUserId}/notifications/{notificationId}/set_read")
    fun setRead(@Path("appUserId") userId: String,
                @Path("notificationId") notificationId: String,
                @Query("mobileToken") token: String): Call<Any>

    @FormUrlEncoded
    @PUT("mobile/user/{appUserId}")
    fun updateUser(@Path("appUserId") userId: String,
                   @Query("mobileToken") token: String,
                   @Field("uuid") uuid: String) : Call<Any>

    companion object {

        var baseUrl = ""

        private var apiServiceInstance: ApiService? = null

        fun getInstance(): ApiService {
            if (apiServiceInstance == null) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY

                val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

                Log.d("BaseURL", "baseurl"+baseUrl);
                val builder = Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client)
                apiServiceInstance = builder.build().create(ApiService::class.java)
            }
            return apiServiceInstance!!
        }
    }
}

