package com.m3.sdk;

import android.content.Context;

import com.google.firebase.FirebaseOptions;
import com.m3.sdk.callbacks.AuthorizationCallback;

public class DynamicPush {

    public static void initWith(Context context, String userId, String userToken, FirebaseOptions firebaseOptions, AuthorizationCallback callback) {
        DynamicPushImpl.Companion.initWith(context, userId, userToken, firebaseOptions, callback);
    }

    public static DynamicPushImpl getInstance(Context context) {
        return DynamicPushImpl.Companion.getInstance(context);
    }
}