package com.m3.sdk.utils

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.util.DisplayMetrics
import android.webkit.MimeTypeMap
import com.m3.sdk.DynamicPush
import com.m3.sdk.activities.AdvertisementActivity

object Helper {

    fun dpToPx(context: Context?, dp: Int): Int {
        if (context == null)
            return 0

        val displayMetrics = context.resources.displayMetrics
        val px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
        return px
    }

    fun getLauncherIconSize(context: Context): Int{
        val activitymanager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val sizeF = activitymanager.launcherLargeIconSize
        return sizeF.toInt()
    }

    fun getUrlIntent(context: Context,
                     url: String?,
                     notificationId: String?,
                     target: String?,
                     type: String?): Intent? {

        val intent: Intent?

        if (url != null && url.isNotEmpty() && notificationId != null && target != null && type != null) {
            intent = Intent(context, AdvertisementActivity::class.java)
            intent.putExtra("url", url)
            intent.putExtra("id", notificationId)
            intent.putExtra("target", target)
            intent.putExtra("type", type)
        }
        else {
            intent = DynamicPush.getInstance(context).defaultAppIntent
        }

        intent?.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP

        return intent
    }

    fun getMimeType(url: String): String? {
        val extension = MimeTypeMap.getFileExtensionFromUrl(url)
        if (extension != null) {
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        }
        return null
    }
}
