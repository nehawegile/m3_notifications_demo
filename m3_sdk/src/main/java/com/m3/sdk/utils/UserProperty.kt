package com.m3.sdk.utils

object UserProperty {

    //core
    var FIREBASE_TOKEN = "firebase_token"

    //device specific
    var OS_NUMBER = "os_number"
    var DEVICE_VENDOR = "device_vendor"
    var DEVICE_MODEL = "device_model"
    var DEVICE_PLATFORM = "device_platform"
    var CONNECTIVITY_TYPE = "connectivity_type"
    var CARRIER = "carrier"

    //app specific
    var APP_VERSION = "app_version"
    var SDK_VERSION = "sdk_version"
    var PACKAGE_NAME = "package_name"
    var PUSH_ENABLED = "push_enabled"

    //user specific
    var NAME = "name"
    var GENDER = "gender"
    var EMAIL = "email"
    var PHONE = "phone"
    var AGE = "age"

    //location specific
    var COUNTRY = "country"
    var CITY = "city"
    var STREET = "street"
    var ZIP_CODE = "zip_code"
    var LANGUAGE = "language"
    var STATE = "state"

    var LATITUDE = "latitude"
    var LONGITUDE = "longitude"
}
