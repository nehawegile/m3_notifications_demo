package com.m3.sdk.utils

import android.app.Activity
import android.app.ActivityManager
import android.app.NotificationManager
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.graphics.Bitmap
import android.widget.RemoteViews
import com.m3.sdk.R
import com.m3.sdk.data.network.*
import com.google.android.gms.common.ConnectionResult
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.DisplayMetrics
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.m3.sdk.BuildConfig
import com.m3.sdk.DynamicPush
import com.m3.sdk.activities.AdvertisementActivity
import com.m3.sdk.models.PushNotification
import com.m3.sdk.services.overlay.DPOverlayService
import com.m3.sdk.services.widget.DPWidgetService

 val KEY_EXTRA_PUSH = "key_extra_push"
 val KEY_EXTRA_TEXT = "key_extra_text"
 val KEY_EXTRA_STOP = "key_extra_stop"
 val KEY_EXTRA_START = "key_extra_start"

fun loadWithGlide(context: Context,
                  url: String,
                  mimeType: String,
                  target: ImageView,
                  onLoaded: (width: Int, height: Int) -> Unit = { w, h -> },
                  onError: (Exception?) -> Unit = { exc -> }) {
    if (mimeType == "image/gif") {
        Glide.with(context)
                .load(url)
                .asGif()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .listener(GifListener ({ width, height ->
                    onLoaded.invoke(width, height)
                }, { error ->
                    onError.invoke(error)
                }))
                .into(target)
    } else {
        Glide.with(context)
                .load(url)
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .listener(object : RequestListener<String, Bitmap> {
                    override fun onException(e: Exception?, model: String?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                        return false
                    }
                    override fun onResourceReady(resource: Bitmap?, model: String?, target: Target<Bitmap>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                        onLoaded(resource?.width!!, resource?.height!!)
                        return false
                    }
                })
                .into(target)
    }
}

 fun getPushFromIntent(intent: Intent?): PushNotification? {
    if (intent != null && intent.hasExtra(KEY_EXTRA_PUSH)) {
        val json = intent.getStringExtra(KEY_EXTRA_PUSH)
        return PushNotification.from(json)
    }
    return null
}

 fun isStopCommand(intent: Intent?): Boolean {
    return intent?.getBooleanExtra(KEY_EXTRA_STOP, false) ?: false
}

 fun isWithText(intent: Intent?): Boolean = intent?.getBooleanExtra(KEY_EXTRA_TEXT, false) ?: false

 fun removeAllNotifications(context: Context) {

    DPWidgetService.stop(context)

    DPOverlayService.stop(context)

    val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    notificationManager.cancelAll()
}

 fun getDeviceID(context: Context): String {
    return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
}

fun api21(body: () -> Unit) {
    if (Build.VERSION.SDK_INT >= 21) {
        body.invoke()
    }
}

fun tryNoException(body: () -> Unit) {
    try {
        body.invoke()
    }
    catch (ignored: Exception) {
        log("Exception ignored: ${ignored.message}")
    }
}

 class GifListener(val onGifReady: (width: Int, height: Int) -> Unit,
                  val onError: (exception: Exception?) -> Unit) : RequestListener<String, GifDrawable> {
    override fun onResourceReady(resource: GifDrawable?, model: String?, target: Target<GifDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
        val width = resource?.firstFrame?.width
        val height = resource?.firstFrame?.height
        if (width != null && height != null) {
            onGifReady.invoke(width, height)
        }
        return false
    }
    override fun onException(e: Exception?, model: String?, target: Target<GifDrawable>?, isFirstResource: Boolean): Boolean {
        onError.invoke(e)
        return false
    }
}

 class LocalGifListener(val onGifReady: () -> Unit) : RequestListener<Int, GifDrawable> {

    override fun onException(e: Exception?, model: Int?, target: Target<GifDrawable>?, isFirstResource: Boolean): Boolean {
        onGifReady.invoke()
        return false
    }

    override fun onResourceReady(resource: GifDrawable?, model: Int?, target: Target<GifDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
        return false
    }
}

 class OnTouch(val body: () -> Unit) : View.OnTouchListener {
    override fun onTouch(view: View?, event: MotionEvent?): Boolean {
        if (event?.action == MotionEvent.ACTION_DOWN) {
            body.invoke()
        }
        return false
    }
}

 class OnMove(val onMove: (x: Int, y: Int) -> Unit,
             val onClick: () -> Unit) : View.OnTouchListener {

    override fun onTouch(view: View?, event: MotionEvent?): Boolean {
        if (event == null)
            return false

        log("TouchEvent: ${event.action} Masked: ${event.actionMasked}")

        if (event.action == MotionEvent.ACTION_MOVE) {
            onMove.invoke(event.rawX.toInt(), event.rawY.toInt())
            return true
        }
        else if (event.action == MotionEvent.ACTION_UP) {
            onClick.invoke()
        }
        return false
    }
}

 fun pxToDp(context: Context?, px: Int): Int {
    if (context == null)
        return 0

    val density = context.resources.displayMetrics.density
    val dp: Float = px / density
    return dp.toInt()
}

 val PLAY_SERVICES_RESOLUTION_REQUEST = 0

// fun isPlayServicesEnabled(context: Context): Boolean {
//
//    val googleApi = GoogleApiAvailability.getInstance()
//    val result = googleApi.isGooglePlayServicesAvailable(context)
//    if (result != ConnectionResult.SUCCESS) {
//        if (googleApi.isUserResolvableError(result) && context is Activity) {
//            googleApi.getErrorDialog(context, result, PLAY_SERVICES_RESOLUTION_REQUEST).show()
//        }
//        return false
//    }
//    return true
//}

 fun getWidgetIconSize(context: Context): Int {
    return Helper.dpToPx(context, 36)
}

 fun getOsNumber(): String {
    val fields = Build.VERSION_CODES::class.java.fields
    val sdk = android.os.Build.VERSION.SDK_INT
    val index = sdk + 1
    var osName: String
    if (fields.size > index) {
        osName = fields[index].name
    }
    else {
        osName = fields[sdk].name
    }

    return "$osName $sdk"
}

 fun getDeviceVendor(): String = Build.MANUFACTURER

 fun getDeviceModel(): String = Build.MODEL

 fun getPackageName(context: Context) = context.packageName

 fun getDevicePlatform(): String = "Android"

 fun getAppVersion(context: Context): String {
    val packageManager = context.packageManager
    val packageName = context.packageName
    return packageManager.getPackageInfo(packageName, 0).versionName
}

fun getSdkVersion(): String = BuildConfig.VERSION_NAME

 fun getCarrier(context: Context): String {
    val telManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    return telManager.networkOperatorName
}

 fun getConnectivityType(context: Context): String {
    val telManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    val networkType = telManager.networkType

    val result = when (networkType) {
        TelephonyManager.NETWORK_TYPE_GPRS -> "2G"
        TelephonyManager.NETWORK_TYPE_EDGE -> "2G"
        TelephonyManager.NETWORK_TYPE_CDMA -> "2G"
        TelephonyManager.NETWORK_TYPE_1xRTT -> "2G"
        TelephonyManager.NETWORK_TYPE_IDEN -> "2G"

        TelephonyManager.NETWORK_TYPE_UMTS -> "3G"
        TelephonyManager.NETWORK_TYPE_EVDO_0 -> "3G"
        TelephonyManager.NETWORK_TYPE_EVDO_A -> "3G"
        TelephonyManager.NETWORK_TYPE_HSDPA -> "3G"
        TelephonyManager.NETWORK_TYPE_HSUPA -> "3G"
        TelephonyManager.NETWORK_TYPE_HSPA -> "3G"
        TelephonyManager.NETWORK_TYPE_EVDO_B -> "3G"
        TelephonyManager.NETWORK_TYPE_EHRPD -> "3G"

        TelephonyManager.NETWORK_TYPE_LTE -> "4G"
        else -> "Unknown"
    }

    return result
}

fun getDPI(context: Context): Float {
    val metric = context.resources.displayMetrics
    val dpi = metric.density * 160
    return dpi
}

 fun getApplicationName(context: Context): String {
    val applicationInfo = context.applicationInfo
    val stringId = applicationInfo.labelRes
    return if (stringId == 0) applicationInfo.nonLocalizedLabel.toString() else context.getString(stringId)
}