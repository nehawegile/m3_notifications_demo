package com.m3.sdk.callbacks;

import java.util.ArrayList;
import java.util.List;

public class HomeScreenCallback {

  private static HomeScreenCallback instance;

  private boolean isOnHomeScreen;

  private List<HomeScreenCallbacks> callbacks = new ArrayList<>();

  public static HomeScreenCallback getInstance() {
    if (instance == null) {
      synchronized (HomeScreenCallback.class) {
        if (instance == null) {
          instance = new HomeScreenCallback();
        }
      }
    }
    return instance;
  }

  public void setCallback(HomeScreenCallbacks homeScreenCallback) {
    callbacks.add(homeScreenCallback);
  }

  public void removeCallback(HomeScreenCallbacks homeScreenCallback) {
    callbacks.remove(homeScreenCallback);
  }

  public void notifyHomeScreenEntered() {
    isOnHomeScreen = true;
    for (HomeScreenCallbacks callback : callbacks) {
      callback.onHomeScreenEntered();
    }
  }

  public void notifyHomeScreenLeaved() {
    isOnHomeScreen = false;
    for (HomeScreenCallbacks callback : callbacks) {
      callback.onHomeScreenLeaved();
    }
  }

  public boolean isOnHomeScreen() {
    return isOnHomeScreen;
  }

  public interface HomeScreenCallbacks {
    void onHomeScreenEntered();

    void onHomeScreenLeaved();
  }
}
