package com.m3.sdk.callbacks

abstract class VerificationCallback {
    abstract fun onSuccess()
    abstract fun onError(reason: String?)
}
