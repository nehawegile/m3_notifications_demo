package com.m3.sdk.callbacks

abstract class AuthorizationCallback {
    abstract fun onSuccess()
    abstract fun onError(reason: String?)
    abstract fun onUserNotVerified()
}
