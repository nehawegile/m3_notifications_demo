package com.m3.sdk.activities

import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import android.widget.TextView
import com.m3.sdk.DynamicPush
import com.m3.sdk.R
import com.m3.sdk.data.network.log
import com.m3.sdk.models.DynamicPushEvent
import com.m3.sdk.models.EventType
import android.net.Uri
import android.os.AsyncTask
import android.support.v4.content.IntentCompat
import android.util.Log
import android.webkit.URLUtil
import com.m3.sdk.utils.removeAllNotifications
import java.net.HttpURLConnection
import java.net.URL


class AdvertisementActivity : AppCompatActivity() {

    private var webView: WebView? = null
    private var progressBar: ProgressBar? = null
    private var urlInvalidText: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        progressBar = findViewById(R.id.progressBar) as ProgressBar

        urlInvalidText = findViewById(R.id.text) as TextView

        webView = findViewById(R.id.webView) as WebView
        webView?.settings?.javaScriptEnabled = true
        webView?.setWebViewClient(WebClient {
            progressBar?.visibility = View.GONE
        })

        val id = intent.getStringExtra("id")
        DynamicPush.getInstance(this).setRead(id)

        val type = intent.getStringExtra("type")

        val target = intent.getStringExtra("target")

        if (target != "list") {
            DynamicPush.getInstance(this)
                    .trackEvent(DynamicPushEvent()
                            .withType(EventType.CLICK)
                            .withTarget(target)
                            .withValue(type))
        }

        removeAllNotifications(this)

        var url = intent.getStringExtra("url")

        var protocal = "http://"

        if (url.contains("https://")) {
            protocal = "https://"
        }

        url = url.replace("http://", "")
        url = url.replace("https://", "")

        if (!url.startsWith("www")) {
            url = "www." + url
        }

        url = protocal + url


        if (URLUtil.isValidUrl(url)) {
            ReachabilityTask(url, { success ->

                if (success) {
                    openUrl(url)
                }
                else {
                    openApp()
                }

            }).execute()

        }
        else {
            openApp()
        }

        intent.removeExtra("id")
        intent.removeExtra("type")
        intent.removeExtra("target")
        intent.removeExtra("url")
    }

    private fun openUrl(url: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(Intent.createChooser(browserIntent, "Open URL"))
        finish()
    }

    private fun openApp() {
        val packageName = this.packageName
        val launchIntent = this.packageManager.getLaunchIntentForPackage(packageName)
        val cn = launchIntent.component
        val mainIntent = IntentCompat.makeRestartActivityTask(cn)
        if (mainIntent != null) {
            var pendingIntent = PendingIntent.getActivity(this, 30, mainIntent, PendingIntent.FLAG_ONE_SHOT)
            pendingIntent.send()
        }

//        val cls = Class.forName("com.m3.sample.activities.StartupActivity")
//        val intent = Intent(this, cls)
//        val pendingIntent = PendingIntent.getActivity(this, 90, intent, 0)
//        pendingIntent.send()
        //finish()
    }

    private class WebClient(val onFinished: () -> Unit) : WebViewClient() {
        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            onFinished.invoke()
        }
    }

    private class ReachabilityTask(val url: String,
                                   val onResult: (Boolean) -> Unit) : AsyncTask<Void, Void, Boolean>() {

        override fun doInBackground(vararg p0: Void?): Boolean {
            try {
                val u = URL(url)
                val conn = u.openConnection() as HttpURLConnection
                val code = conn.responseCode
                log("Response code = $code")
                return code == 200 || code == 302
            }
            catch (exc: Exception) {
                return false
            }
        }

        override fun onPostExecute(success: Boolean) {
            super.onPostExecute(success)
            onResult.invoke(success)
        }
    }

}
