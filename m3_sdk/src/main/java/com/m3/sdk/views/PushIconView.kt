package com.m3.sdk.views

import android.content.Context
import android.media.RingtoneManager
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.webkit.WebView
import android.widget.*
import com.m3.sdk.R
import com.m3.sdk.models.PushNotification
import com.m3.sdk.utils.Helper
import com.m3.sdk.utils.tryNoException



 class PushIconView : RelativeLayout {

    //constructor that we actually use
    constructor(context: Context,
                pushNotification: PushNotification?,
                closeCallback: () -> Unit) : super(context) {

        this.pushNotification = pushNotification
        this.closeCallback = closeCallback
        applyViewStyle()
    }

    //default constructors
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    //sub views
    var root: View
    var contentText: TextView
    private var contentMedia: RelativeLayout
    private var closeImage: ImageView

    var contentImage: ImageView
    var contentVideo: VideoView
    var linkButton: Button
    var playButton: ImageView
     var pauseButton: ImageView
     var muteButton: ImageView
     var unMuteButton: ImageView
     var cancelButton: Button
     var cardViewRoot : View
    //utils
    private var pushNotification: PushNotification? = null
    private var closeCallback: () -> Unit? = {}
    private val smallTextSize = 10F
    private val bigTextSize = 14F

    //error messages
    private var errorNoAnimation = "Animation is not exist or contains empty url"
    private var errorCantLoad = "Animation can`t be loaded. "

    init {
        LayoutInflater.from(context).inflate(R.layout.view_push_icon, this)

        root = findViewById(R.id.root)
        contentMedia = findViewById(R.id.contentMedia)
        contentImage = findViewById(R.id.contentImage)
        contentVideo = findViewById(R.id.contentVideo)
        contentText = findViewById(R.id.contentText)
        closeImage = findViewById(R.id.closeImage)
        linkButton = findViewById(R.id.link_button)
        playButton = findViewById(R.id.play)
        pauseButton = findViewById(R.id.pause)
        muteButton = findViewById(R.id.mute)
        unMuteButton = findViewById(R.id.unmute)
        closeImage.visibility = View.GONE
        cancelButton = findViewById(R.id.cancel_button)
        cardViewRoot = findViewById(R.id.cardViewRoot)

        cancelButton.setOnClickListener {

            animate(false, {
                closeCallback.invoke()
            })
        }

    }

    private fun applyViewStyle() {
        if (pushNotification != null) {
            if (pushNotification!!.isCloseAble()) {
                //root.setBackgroundResource(R.drawable.bg_notification)
                //header.visibility = View.VISIBLE
                closeImage.visibility = View.GONE
                closeImage.setOnClickListener {
                    animate(false, {
                        closeCallback.invoke()
                    })
                }
            }
        }
    }

    fun animate(show: Boolean, onDissapear: () -> Unit = {}) {
        val delay = 300L
//        val scale = if (show) 1F else .5F
//        val alpha = if (show) 1F else 0F
//
//        root
//                .animate()
//                .scaleX(scale)
//                .scaleY(scale)
//                .alpha(alpha)
//                .setDuration(delay)
//                .start()
//
        postDelayed({
            onDissapear.invoke()
        }, delay + 100)
    }

//    private fun playSound() {
//        tryNoException {
//            val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
//            RingtoneManager.getRingtone(context, uri).play()
//        }
//    }
//
//    private fun setImageSize(width: Int, height: Int) {
//        contentImage.layoutParams.width = Helper.dpToPx(context, width)
//        contentImage.layoutParams.height = Helper.dpToPx(context, height)
//        contentImage.scaleType = ImageView.ScaleType.CENTER_CROP
//        contentImage.requestLayout()
//    }
}
