package com.m3.sdk

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import java.util.*

class DynamicPushConfig(context: Context) {

    val preferences: SharedPreferences
    init {
        preferences = PreferenceManager.getDefaultSharedPreferences(context)
    }

    //keys
    private val keyUserProperties = "dp_sdk_key_user_properties"
    private val keyUserCredentials = "dp_sdk_key_user_credentials"
    private val keyGpsEnabled = "dp_sdk_key_gps_enabled"
    private val keyLoggedOut = "dp_sdk_key_user_logout"

    private class PropertiesHolder() {
        var properties: HashMap<String, String> = HashMap()

        constructor(map: HashMap<String, String>) : this() {
            properties = map
        }
    }
    private var propertiesHolder: PropertiesHolder? = null

    fun saveUserProperties(properties: Map<String, String>) {
        if (propertiesHolder == null) {
            propertiesHolder = PropertiesHolder(getUserProperties())
        }
        propertiesHolder!!.properties.putAll(properties)
        preferences.edit().putString(keyUserProperties, gson.toJson(propertiesHolder)).commit()
    }

    fun setLoggedOut(what: Boolean) = preferences.edit().putBoolean(keyLoggedOut, what).apply()

    fun isLoggedOut(): Boolean = preferences.getBoolean(keyLoggedOut, false)

    fun getUserProperties(): HashMap<String, String> {
        val json = preferences.getString(keyUserProperties, "")
        if (json.isNotEmpty()) {
            val holder = gson.fromJson(json, PropertiesHolder::class.java)
            propertiesHolder = holder
            return holder.properties
        }
        return HashMap()
    }

    class UserCredentials(var id: String, var token: String)

    fun saveUserCredentials(credentials: UserCredentials) {
        preferences.edit().putString(keyUserCredentials, gson.toJson(credentials)).apply()
    }

    fun getUserCredentials(): UserCredentials {
        val json = preferences.getString(keyUserCredentials, "")
        if (json.isNotEmpty()) {
            val credentials = gson.fromJson(json, UserCredentials::class.java)
            return credentials
        }
        return UserCredentials("", "")
    }

    fun isGpsEnabled(): Boolean {
        return preferences.getBoolean(keyGpsEnabled, true)
    }

    fun setGpsEnabled(enabled: Boolean) {
        preferences.edit().putBoolean(keyGpsEnabled, enabled).apply()
    }

    companion object {

        private val gson = Gson()

        var instance: DynamicPushConfig? = null

        fun getInstance(context: Context): DynamicPushConfig {
            if (instance == null) {
                instance = DynamicPushConfig(context)
            }
            return instance!!
        }
    }
}
