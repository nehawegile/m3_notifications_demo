package com.m3.sdk.services.firebase

import android.os.Handler
import android.util.Log
import com.m3.sdk.DynamicPush
import com.m3.sdk.data.network.log
import com.m3.sdk.models.DynamicPushEvent
import com.m3.sdk.models.EventTarget
import com.m3.sdk.models.EventType
import com.m3.sdk.models.PushNotification
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

 class DPMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)

        Log.d("hello guys", "message arrived")

        val pushNotification = PushNotification.from(message)
        pushNotification.show(this)

        pushNotification.displayType?.forEach {
            DynamicPush.getInstance(this)
                    .trackEvent(DynamicPushEvent()
                            .withType(EventType.RECEIVE)
                            .withTarget(EventTarget.NOTIFICAION)
                            .withValue(it.toString()))
        }
    }
}
