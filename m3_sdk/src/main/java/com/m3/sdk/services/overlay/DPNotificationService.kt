package com.m3.sdk.services.overlay

import android.app.*
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Icon
import android.media.RingtoneManager
import android.support.v4.content.ContextCompat
import com.m3.sdk.R
import com.m3.sdk.models.PushNotification
import com.m3.sdk.utils.*

 fun playSound(context: Context) {
    val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
    val ringtone = RingtoneManager.getRingtone(context, notification)
    ringtone.play()
}

@SuppressWarnings("all")
 fun showDefaultNotification(context: Context, pushNotification: PushNotification?) {

    val message: String? = pushNotification?.message

    if (message.isNullOrEmpty()) {
        playSound(context)
        return
    }

    val notificationManager = context.getSystemService(Service.NOTIFICATION_SERVICE) as NotificationManager
    notificationManager.cancelAll()

    val contentIntent = Helper.getUrlIntent(context,
            pushNotification?.url,
            pushNotification?._id,
            "notification",
            "text")

//    val pendingIntent = PendingIntent.getActivity(context, 50, contentIntent, 0)
//
//    val icon = BitmapFactory.decodeResource(context.resources, R.drawable.dp_logo)
//
//    val drawable = context.packageManager.getApplicationIcon(com.m3.sdk.utils.getPackageName(context)) as BitmapDrawable
//    val bitmap: Bitmap = drawable.bitmap
//
//    val notificationBuilder = Notification.Builder(context)
//            .setContentTitle(getApplicationName(context))
//            .setContentText(message)
//            .setSmallIcon(Icon.createWithBitmap(icon))
//            .setLargeIcon(Icon.createWithBitmap(icon))
//            .setAutoCancel(true)
//            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
//            .setContentIntent(pendingIntent)
//    api21 {
//        notificationBuilder.setColor(ContextCompat.getColor(context, R.color.textNotificationBg))
//    }
//
//    notificationManager.notify(pushNotification!!.ID, notificationBuilder.build())
}