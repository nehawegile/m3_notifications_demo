package com.m3.sdk.services.location

import android.app.Activity
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.*
import android.os.Bundle
import android.os.IBinder
import com.m3.sdk.DynamicPush
import com.m3.sdk.DynamicPushImpl
import com.m3.sdk.data.network.log
import com.m3.sdk.utils.*
import java.util.*

 class DPLocationService : Service() {

    override fun onBind(p0: Intent?): IBinder? = null

    override fun onStartCommand(nullableIntent: Intent?, flags: Int, startId: Int): Int {

        val intent = nullableIntent
        val shouldHandle = intent != null && intent.hasExtra(KEY_EXTRA_START)

        if (shouldHandle) {
            val start = intent!!.getBooleanExtra(KEY_EXTRA_START, false)
            val logState = if (start) "started" else "stopped"
            log("DPLocationService $logState")
            enableLocationService(start)
        }

        return START_REDELIVER_INTENT
    }

    private var locationManager: LocationManager? = null
    private fun getLocationManager(): LocationManager {
        if (locationManager == null) {
            locationManager = getSystemService(Activity.LOCATION_SERVICE) as LocationManager
        }
        return locationManager!!
    }

    private fun getBestProvider(): String {
        val criteria = Criteria()
        return getLocationManager().getBestProvider(criteria, false)
    }

    private var locationListener: LocationListener? = null
    private fun getLocationListener(): LocationListener {
        if (locationListener == null) {
            locationListener = object : LocationListener {
                override fun onProviderDisabled(p0: String?) {}
                override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {}
                override fun onProviderEnabled(p0: String?) {}

                override fun onLocationChanged(loc: Location?) {

                    try {

                        val properties = HashMap<String, String>()
                        val location = loc


                        if (location != null) {

                            properties.put(UserProperty.LATITUDE, location.latitude.toString())
                            properties.put(UserProperty.LONGITUDE, location.longitude.toString())

                            val address = decodeLocation(location)
                            if (address != null) {

                                val country = address.countryName
                                if (!country.isNullOrEmpty()) {
                                    properties.put(UserProperty.COUNTRY, country)
                                }

                                val city = address.locality
                                if (!city.isNullOrEmpty()) {
                                    properties.put(UserProperty.CITY, city)
                                }

                                val street = address.getAddressLine(0)
                                if (!street.isNullOrEmpty()) {
                                    properties.put(UserProperty.STREET, street)
                                }

                                val zipCode = address.postalCode
                                if (!zipCode.isNullOrEmpty()) {
                                    properties.put(UserProperty.ZIP_CODE, zipCode)
                                }

                                val language = address.locale.displayLanguage
                                if (!language.isNullOrEmpty()) {
                                    properties.put(UserProperty.LANGUAGE, language)
                                }

                                val state = parseState(address)
                                if (!state.isNullOrEmpty()) {
                                    properties.put(UserProperty.STATE, state!!)
                                }
                            }
                        }

                        DynamicPush.getInstance(this@DPLocationService).setUserProperties(properties)

                        val listener = DynamicPush.getInstance(this@DPLocationService).getLocationUplodatedListener()
                        listener?.onLocationUpdated(location)


                    } catch (exc: Exception) {}
                }
            }
        }
        return locationListener!!
    }

    private fun parseState(address: Address): String? {
        try {
            val line = address.getAddressLine(1) ?: "-"
            val parts = line.split(", ")
            if (parts.size > 1) {
                val stateIndex = parts[1]
                val stateIndexSplitted = stateIndex.split(" ")
                val state = stateIndexSplitted[0]
                return state
            }
            return null
        }
        catch (exc: Exception) {
            return null
        }

    }

    private fun enableLocationService(enable: Boolean) {
        try {
            if (enable && getBestProvider() != null) {
                getLocationManager().requestLocationUpdates(getBestProvider(), minTime, minDistance, getLocationListener())
            } else {
                getLocationManager().removeUpdates(getLocationListener())
            }
        } catch (exc: Exception) {}
    }

    private fun decodeLocation(location: Location): Address? {
        val addresses = getGeocoder().getFromLocation(location.latitude, location.longitude, 1)
        if (addresses.size > 0) {
            return addresses[0]
        }
        else {
            return null
        }
    }

    private var geocoder: Geocoder? = null
    private fun getGeocoder(): Geocoder {
        if (geocoder == null) {
            geocoder = Geocoder(this, Locale.ENGLISH)
        }
        return geocoder!!
    }

    companion object {
        fun start(context: Context) = startService(context, true)

        fun stop(context: Context) = startService(context, false)

        private val minTime = 0L //20000L

        private val minDistance = 1F

        private fun startService(context: Context, start: Boolean) {
            val intent = Intent(context, DPLocationService::class.java)
            intent.putExtra(KEY_EXTRA_START, start)
            context.startService(intent)
        }
    }
}
