package com.m3.sdk.services.widget

import android.app.PendingIntent
import android.app.Service
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.PixelFormat
import android.graphics.drawable.Animatable
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.support.v4.content.IntentCompat
import android.util.Log
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RemoteViews
import com.m3.sdk.DynamicPush
import com.m3.sdk.R
import com.m3.sdk.data.network.log
import com.m3.sdk.models.PushNotification
import com.m3.sdk.services.overlay.showDefaultNotification
import com.m3.sdk.utils.*
import java.util.*
import kotlin.concurrent.fixedRateTimer

 class DPWidgetService : Service() {

    override fun onBind(p0: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val push = getPushFromIntent(intent)
        val stop = isStopCommand(intent)

        if (push != null && push.hasAnimation()) {
            displayAnimation(push, isWithText(intent))
        }
        else if (stop) {
            stopCapturing()
        }
        else {

        }

        return START_REDELIVER_INTENT
    }

    private fun displayAnimation(push: PushNotification, withText: Boolean) {
//        loadWithGlide(this,
//                push.animation?.url!!,
//                getGifView(),
//                onLoaded = { width, height ->
//
//                    startCapturing(push)
//                    val duration = push.getDisplayDuration()
//                    handler.postDelayed({
//                        stopCapturing()
//                    }, duration)
//
//                    val loopDelay = push.getLoopDelay()
//                    if (loopDelay > 0) {
//                        startLoopDelay(push.getAnimationDuration(), loopDelay)
//
//                    }
//
//                    if (withText) {
//                        showDefaultNotification(this, push)
//                    }
//                },
//                onError = { error ->
//                    //shit happens
//                    log("${error?.message}")
//                })

    }

    private var isRunning = false

    private var timer: Timer? = null
    private fun startCapturing(push: PushNotification) {
        isRunning = true

        timer = fixedRateTimer(period = captureDelay, action = {
            if (isRunning) {
                captureGif(push)
            }
        })
    }

    private var loopDelayTimer: Timer? = null
    private fun startLoopDelay(animationDuration: Long, loopDelay: Long) {
        loopDelayTimer = fixedRateTimer(period = animationDuration + loopDelay, initialDelay = animationDuration, action = {
            stopGif()

            handler.postDelayed({
                startGif()
            }, loopDelay)
        })
    }

    private fun stopGif() {
        val gif = getGifView().drawable
        if (gif is Animatable && isRunning) {
            gif.stop()
        }
    }

    private fun startGif() {
        val gif = getGifView().drawable
        if (gif is Animatable && isRunning) {
            gif.start()
        }
    }

    private fun stopCapturing() {
        isRunning = false

        timer?.cancel()
        loopDelayTimer?.cancel()

        displayLogo()
    }

    private fun captureGif(push: PushNotification) {
        val bitmap = Bitmap.createBitmap(getGifView().width, getGifView().height, Bitmap.Config.RGB_565)
        val canvas = Canvas(bitmap)
        getGifView().draw(canvas)

        updateWidget(bitmap, getClickIntent(push))
    }

    private fun displayLogo() {
        val drawable = packageManager.getApplicationIcon(com.m3.sdk.utils.getPackageName(this)) as BitmapDrawable
        val bitmap: Bitmap = drawable.bitmap
        updateWidget(bitmap)
    }

    private var gifTargetView: ImageView? = null
    private fun getGifView(): ImageView {
        if (gifTargetView == null) {
            gifTargetView = ImageView(this)
            gifTargetView?.translationX = -1000F
            val params = WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_TOAST,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT)
            params.x = 0
            params.y = 0
            params.width = gifQuality
            params.height = gifQuality

            val frame = FrameLayout(this)
            frame.addView(gifTargetView)

            val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager

            var mHandler: Handler? = Handler()
            var runnable = Runnable {
                if (frame.windowToken == null) {
                    windowManager.addView(frame, params)
                }
            }
            mHandler?.post(runnable)
        }
        return gifTargetView!!
    }

    private fun updateWidget(bitmap: Bitmap, onClickIntent: PendingIntent? = null) {
        val widgetManager = AppWidgetManager.getInstance(this)
        val ids = widgetManager.getAppWidgetIds((ComponentName(this, DPWidget::class.java)))
        if (ids.isNotEmpty()) {
            val views = RemoteViews(packageName, R.layout.view_widget_icon)
            views.setImageViewBitmap(R.id.imageView, bitmap)
            views.setTextViewText(R.id.textView, getApplicationName(this))
            if (onClickIntent != null) {
                views.setOnClickPendingIntent(R.id.widgetRoot, onClickIntent)
            }
            else {
                val openAppIntent = getOpenAppIntent()
                if (openAppIntent != null) {
                    views.setOnClickPendingIntent(R.id.widgetRoot, openAppIntent)
                }
            }
            widgetManager.updateAppWidget(ids, views)
        }
    }

    private fun getOpenAppIntent(): PendingIntent? {
        val packageName = this.packageName
        val launchIntent = this.packageManager.getLaunchIntentForPackage(packageName)
//        val className = launchIntent.component.className
//        val cls = Class.forName("com.m3.sample.activities.StartupActivity")
//        val intent = Intent(this, cls)
        val cn = launchIntent.component
        val mainIntent = IntentCompat.makeRestartActivityTask(cn)
        if (mainIntent != null) {
            return PendingIntent.getActivity(this, 30, mainIntent, PendingIntent.FLAG_ONE_SHOT)
        }
        return null
    }

    private fun getClickIntent(push: PushNotification): PendingIntent {
        val clickIntent = Helper.getUrlIntent(this, push.url, push._id, "widget", "dpi")
        return PendingIntent.getActivity(this, 10, clickIntent, 0)
    }

    companion object {

        private val gifQuality = 150

        private val captureDelay = 100L

        private val handler = Handler(Looper.getMainLooper())

        fun start(context: Context, push: PushNotification, withText: Boolean = false) {
            val intent = Intent(context, DPWidgetService::class.java)
            intent.putExtra(KEY_EXTRA_PUSH, push.toJson())
            intent.putExtra(KEY_EXTRA_TEXT, withText)
            context.startService(intent)
         }

        fun stop(context: Context) {
            val intent = Intent(context, DPWidgetService::class.java)
            intent.putExtra(KEY_EXTRA_STOP, true)
            context.startService(intent)
        }
    }
}
