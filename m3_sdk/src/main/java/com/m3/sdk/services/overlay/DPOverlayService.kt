package com.m3.sdk.services.overlay

import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PixelFormat
import android.graphics.Rect
import android.graphics.drawable.Animatable
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.*
import com.m3.sdk.models.PushNotification
import com.m3.sdk.utils.*
import com.m3.sdk.views.PushIconView
import java.util.*
import kotlin.concurrent.fixedRateTimer
import android.media.AudioManager.STREAM_MUSIC
import android.os.*
import android.R.attr.gravity
import android.view.WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE



//import android.support.v4.media.session.MediaControllerCompat.setMediaController

class DPOverlayService : Service() {

    private var windowManager: WindowManager? = null
    private var layoutParams: WindowManager.LayoutParams? = null
    private var lastX: Int = 0
    private var lastY: Int = 0
    private var firstX: Int = 0
    private var firstY: Int = 0
    private var screenWidth: Int = 0
    private var screenHeight: Int = 0
    private var isWasMoved: Boolean = false
    private var isDownTouchPerformed: Boolean = false
    private var isTouchConsumedByMove: Boolean = false
    var mediaplayer : MediaPlayer = MediaPlayer()

    private val mBinder = DPOverlayServiceLocalBinder()
    override fun onBind(p0: Intent?): IBinder? {
        return mBinder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val push = getPushFromIntent(intent)
        val stop = isStopCommand(intent)

        if (push != null && push.hasAnimation()) {
            showOverlay(push, isWithText(intent))
        }
        else if (stop) {
            hideOverlay(true)
        }
        else {

        }

        return START_REDELIVER_INTENT
    }
    override fun onCreate() {
        super.onCreate()

        windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager

        val displayMetrics = DisplayMetrics()
        windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        screenWidth = displayMetrics.widthPixels
        screenHeight = displayMetrics.heightPixels

    }
    private fun onClickPlayPause(push: PushNotification) {
        if(pushView!!.playButton.visibility== View.VISIBLE) {
            pushView!!.playButton.visibility = View.INVISIBLE
//            pushView!!.pauseButton.visibility = View.GONE
            getPushView(push).contentVideo.start()
//            getPushView(push).contentText.visibility = View.GONE
        }
        else if(pushView!!.playButton.visibility== View.INVISIBLE)
        {
            pushView!!.playButton.visibility = View.VISIBLE
//            pushView!!.pauseButton.visibility = View.GONE
            getPushView(push).contentVideo.pause()
//            getPushView(push).contentText.visibility = View.VISIBLE
        }
    }

    private fun setVolume(volume: Float, mediaPlayer: MediaPlayer) {

        if(mediaPlayer!=null)
            mediaPlayer.setVolume(volume,volume)
    }


    private fun onClicked(push: PushNotification) {
        Log.d("hello guys", "clicked nova")

        val intent = Helper.getUrlIntent(this,
                push.url,
                push._id,
                "notification",
                push.getNovaType())

//        val pendingIntent = PendingIntent.getActivity(this, (Math.random() * 1000).toInt(), intent, 0)
//        pendingIntent.send()
        val LaunchIntent = packageManager.getLaunchIntentForPackage("com.m3").putExtra("product_1","1")
        startActivity(LaunchIntent)

        hideOverlay()
    }
    private var pushView: PushIconView? = null
    private fun getPushView(push: PushNotification, recreate: Boolean = false): PushIconView {
        if (recreate) {
            pushView = null
        }

        if (pushView == null) {
            pushView = PushIconView(this, push, {
                hideOverlay(true)
                DPOverlayService.stop(this)
            })

            pushView!!.setOnTouchListener(View.OnTouchListener { view, motionEvent ->  onTouch(motionEvent, push)})
            pushView!!.linkButton.setOnClickListener(View.OnClickListener { v: View ->
                onClicked(push)
            })
            pushView!!.playButton.setOnClickListener(View.OnClickListener { v: View ->
                onClickPlayPause(push)
            })
//            pushView!!.pauseButton.setOnClickListener(View.OnClickListener { v: View ->
//                onClickPlayPause(push)
//            })


            pushView!!.contentVideo.setOnPreparedListener(object : MediaPlayer.OnPreparedListener {

                override fun onPrepared(mp: MediaPlayer) {

                    mp.setVolume(0f, 0f)
                    mp.start()
                    Toast.makeText(this@DPOverlayService,"CallpreparListner",Toast.LENGTH_LONG).show()

                }
            })
//            Display display = getWindowManager().getDefaultDisplay();
//                   var width: Int = display.getWidth()
//                   int height = display.getHeight();
//
//                //   videoview.setLayoutParams(new FrameLayout.LayoutParams(550,550));
//
//                   videoview.setLayoutParams(new FrameLayout.LayoutParams(width,height));


            pushView!!.playButton.setOnClickListener(View.OnClickListener { v:View->
                if(pushView!!.contentVideo.isPlaying)
                {
                    pushView!!.playButton.visibility=View.VISIBLE
                    pushView!!.contentVideo.pause()
//                    getPushView(push).contentText.visibility = View.VISIBLE
                }
                else {
                    pushView!!.playButton.visibility = View.INVISIBLE
                    pushView!!.contentVideo.start()
//                    getPushView(push).contentText.visibility = View.GONE
                }
            })
        }
        return pushView!!
    }

    private fun getPushIconRect(push: PushNotification): Rect {
        val loc = intArrayOf(0, 0)
        getPushView(push).getLocationOnScreen(loc)
        val pushIconWidth: Int = getPushView(push).width
        val pushIconHeight: Int = getPushView(push).height
        return Rect(loc[0], loc[1], loc[0] + pushIconWidth, loc[1] + pushIconHeight)
    }

    private fun onTouch(event: MotionEvent?, push: PushNotification): Boolean {
        Log.d("onTouch",""+event)
        if (event == null
                || layoutParams == null) {
            return false
        }

        try {
            if (!getPushIconRect(push).contains(event.rawX.toInt(), event.rawY.toInt())) {
                if (event.actionMasked == MotionEvent.ACTION_DOWN) {
                    return false
                }

                if (event.actionMasked != MotionEvent.ACTION_MOVE) {
                    return false
                }
            }
        }
        catch (ignored: Exception) {
        }

        val totalDeltaX = lastX - firstX
        val totalDeltaY = lastY - firstY
        val params: WindowManager.LayoutParams = layoutParams!!

        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                isDownTouchPerformed = true
                isWasMoved = false

                lastX = event.rawX.toInt()
                lastY = event.rawY.toInt()
                firstX = lastX
                firstY = lastY
            }
            MotionEvent.ACTION_UP -> {
                isDownTouchPerformed = false
                val deltaX = event.rawX.toInt() - firstX
                val deltaY = event.rawY.toInt() - firstY
                if (!isWasMoved || (Math.abs(deltaX) <= 10 || Math.abs(deltaY) <= 10)) {
//                    onClicked(push)
                }
            }
            MotionEvent.ACTION_MOVE -> {
                val deltaX = event.rawX.toInt() - lastX
                val deltaY = event.rawY.toInt() - lastY
                lastX = event.rawX.toInt()
                lastY = event.rawY.toInt()

                if (layoutParams != null && event.pointerCount == 1 && (Math.abs(totalDeltaX) >= 3 || Math.abs(totalDeltaY) >= 3)) {
                    params.x += deltaX
                    params.y += deltaY

                    if (isDownTouchPerformed) {
                        isWasMoved = true
                        isTouchConsumedByMove = true
                        windowManager?.updateViewLayout(getPushView(push), params)
                    }
                    else {
                        isTouchConsumedByMove = false
                    }
                }
                else {
                    isTouchConsumedByMove = false
                }
            }
        }
        return isTouchConsumedByMove
    }

    private fun showOverlay(push: PushNotification, withText: Boolean) {
        layoutParams = WindowManager.LayoutParams()

        val layoutFlag: Int;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            layoutFlag=WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        else
            layoutFlag=WindowManager.LayoutParams.TYPE_TOAST


//        layoutParams = WindowManager.LayoutParams(
//                WindowManager.LayoutParams.WRAP_CONTENT,
//                WindowManager.LayoutParams.WRAP_CONTENT,
//                layoutFlag,
//                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
//                PixelFormat.TRANSLUCENT)

//        params.gravity = Gravity.BOTTOM or Gravity.END
//        params.gravity = Gravity.TOP or Gravity.LEFT

        layoutParams?.format = PixelFormat.TRANSLUCENT
        layoutParams?.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE

//        layoutParams?.type = WindowManager.LayoutParams.TYPE_TOAST
        layoutParams?.type = layoutFlag
        layoutParams?.gravity = getIconGravity(push)

//        if(Build.)

        applyBottomGravity(push)

        val maxWidth = getMaxWidth(push)
        val maxHeight = getMaxHeight(push)

        layoutParams?.width = maxWidth


        hideOverlay()
        addOnScreen(push)

        val mimeType = Helper.getMimeType(push.animation?.url!!)

        if (mimeType != null && mimeType.startsWith("image")) {
            loadWithGlide(this,
                    push.animation?.url!!,
                    mimeType,
                    getPushView(push).contentImage,
                    onLoaded = { width, height ->

                        try {
                            windowManager?.updateViewLayout(getPushView(push), getParams(true, maxWidth, maxHeight))
                        }
                        catch (exc: Exception) {
                        }

                        setImageSize(getPushView(push).contentImage, maxWidth, getMaxImageHeight(push))
                        setVideoSize(getPushView(push).contentVideo, maxWidth, getMaxImageHeight(push))
                        getPushView(push).contentImage.visibility = View.VISIBLE
                        getPushView(push).contentVideo.visibility = View.GONE

                        setText(getPushView(push).contentText, push.message ?: "")

                        val duration = push.getDisplayDuration()
                        if (mimeType == "image/gif") {
                            getPushView(push).animate(true)
                            handler.postDelayed({
                                hideOverlay()
                            }, duration)
                        }

                        val loopDelay = push.getLoopDelay()

                        if (loopDelay > 0) {
                            startLoopDelay(push, loopDelay)
                        }

                        if (withText) {
                            showDefaultNotification(this, push)
                        }
                    },
                    onError = { error ->

                    })
        } else if (mimeType != null && mimeType.startsWith("video")) {
            Log.d("hello guys", "loading video.....")

             var isFirstTime: Boolean = true


            windowManager?.updateViewLayout(getPushView(push), getParams(true, maxWidth, maxHeight))

            setImageSize(getPushView(push).contentImage, maxWidth, getMaxImageHeight(push))
            setVideoSize(getPushView(push).contentVideo, maxWidth, getMaxImageHeight(push))
            getPushView(push).contentImage.visibility = View.GONE
            getPushView(push).contentVideo.visibility = View.VISIBLE
            getPushView(push).playButton.visibility=View.VISIBLE
            getPushView(push).pauseButton.visibility = View.GONE
            getPushView(push).muteButton.visibility= View.VISIBLE
            getPushView(push).unMuteButton.visibility= View.INVISIBLE

            getPushView(push).unMuteButton.setOnClickListener(View.OnClickListener {

//                val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
//                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
                if(mediaplayer!=null)
                    mediaplayer.setVolume(0f,0f)
                    getPushView(push).unMuteButton.visibility= View.GONE
                    getPushView(push).muteButton.visibility= View.VISIBLE

            })

            getPushView(push).muteButton.setOnClickListener(View.OnClickListener {
//                val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
//                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 20, 0);
                if(mediaplayer!=null)
                    mediaplayer.setVolume(1.0f,1.0f)
                    getPushView(push).muteButton.visibility= View.GONE
                    getPushView(push).unMuteButton.visibility= View.VISIBLE

            })

            setText(getPushView(push).contentText, push.message ?: "")

//            val mediaController =MediaController(this)
//            mediaController.setAnchorView( getPushView(push).contentVideo)
//            getPushView(push).contentVideo.setMediaController(mediaController)

//            getPushView(push).contentVideo.setVideoPath(push.animation?.url!!)
            getPushView(push).contentVideo.setVideoURI(Uri.parse(push.animation?.url!!))
//            getPushView(push).contentVideo.setZOrderOnTop(true)

            getPushView(push).contentVideo.setOnPreparedListener { m ->

                Log.d("PREPARED","pREPARED")
                getPushView(push).contentVideo.setBackgroundColor(Color.TRANSPARENT)
//                getPushView(push).contentVideo.start()
                //Just seek
                getPushView(push).contentVideo.seekTo(1000)
                if (m.isPlaying) {
                    m.stop()
                    m.release()
                    //m = MediaPlayer()
                }
//                getPushView(push).muteButton.visibility= View.INVISIBLE
                getPushView(push).playButton.visibility=View.INVISIBLE

//                mediaplayer =m
                if(isFirstTime) {
                    m.setVolume(0f, 0f)
                    isFirstTime = false;
                }
                m.isLooping = false
                m.start()
                mediaplayer =m
            }

            getPushView(push).contentVideo.setOnCompletionListener { mp ->
                getPushView(push).playButton.visibility= View.VISIBLE
//                getPushView(push).contentVideo.stopPlayback()
            }

            val loopDelay = push.getLoopDelay()

            if (loopDelay > 0) {
                startLoopDelay(push, loopDelay)
            }

            if (withText) {
                showDefaultNotification(this, push)
            }
        }





    }









    private fun setImageSize(image: ImageView, width: Int, height: Int) {
        image.layoutParams.width = width
        image.layoutParams.height = height
        image.scaleType = ImageView.ScaleType.CENTER_CROP
        image.requestLayout()
    }

    private fun setVideoSize(view: VideoView, width: Int, height: Int) {
        view.layoutParams.width = width
        view.layoutParams.height = height
        view.requestLayout()
    }

    private fun setText(textView: TextView, text: String) {
        textView.text = text
    }

    private var loopDelayTimer: Timer? = null
    private fun startLoopDelay(push: PushNotification, loopDelay: Long) {
        val animationDuration = push.getAnimationDuration()

        loopDelayTimer = fixedRateTimer(period = animationDuration + loopDelay, initialDelay = animationDuration, action = {
            stopGif(push)

            handler.postDelayed({
                startGif(push)
            }, loopDelay)
        })
    }

    private fun stopGif(push: PushNotification) {
        val gif = getPushView(push).contentImage.drawable
        if (gif is Animatable) {
            gif.stop()
        }
    }

    private fun startGif(push: PushNotification) {
        val gif = getPushView(push).contentImage.drawable
        if (gif is Animatable) {
            gif.start()
        }
    }

    private fun hideOverlay(noAnim: Boolean = false) {
        val pv = pushView
        if (pv != null && pv.parent != null) {
                if (noAnim) {
                    try {
                        windowManager?.removeView(pv)
                    }
                    catch (exc: Exception) {

                    }
                }
                else {
                    pv.animate(false, {
                        try {
                            windowManager?.removeView(pv)
                        }
                        catch (exc: Exception) {

                        }
                    })
                }
        }
        handler.removeCallbacksAndMessages(null)
        loopDelayTimer?.cancel()
    }

//    fun resumeOverlay() {
//        val pv = pushView
//        if (pv != null && pv.parent != null) {
//            pv.visibility = View.VISIBLE
//        }
//    }

    fun pauseOverlay() {
        val pv = pushView
        if (pv != null && pv.parent != null) {
            pv.visibility = View.GONE
        }
    }

    private fun getParams(touchable: Boolean, srcWidth: Int = 0, srcHeight: Int = 0): WindowManager.LayoutParams? {
        /*
        layoutParams?.flags =
                if (touchable) WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                else WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                */
        layoutParams?.width = srcWidth
        layoutParams?.height = srcHeight

        return layoutParams
    }

    private fun addOnScreen(push: PushNotification) {
        var view : PushIconView = getPushView(push, true)
        if (view.windowToken == null) {
            windowManager?.addView(view, getParams(false))
        }
    }

    private fun isLandscape() = screenWidth > screenHeight

    private fun getMaxWidth(push: PushNotification): Int {
        var maxWidth =
                if (push.isMegaNova()) {
                    screenWidth
                }
                else if (push.isSuperNova()) {

                    if (isLandscape()) {
                        screenWidth / 2
                    }
                    else {
                        screenWidth
                    }

                }
                else {
                    Helper.dpToPx(this, 100)
                }

        if (push.isSuperNova()) {
            maxWidth = (maxWidth * .8).toInt()
        }

        return maxWidth
    }

    private fun getMaxImageHeight(push: PushNotification): Int {
        if (push.isMegaNova()) {
            return (screenHeight * 0.5).toInt()
        }
        else if (push.isSuperNova()) {
            return (screenHeight * 0.3).toInt()
        }
        else {
            return Helper.dpToPx(this, 100)
        }
    }

    private fun getMaxHeight(push: PushNotification): Int {
        val maxHeight =
                if (push.isMegaNova()) {
                    screenHeight
                }
                else if (push.isSuperNova()) {

                    if (isLandscape()) {
                        screenHeight
                    }
                    else {
                        screenHeight / 4 * 3
                    }

                }
                else {
                    Helper.dpToPx(this, 100)
                }

        return maxHeight
    }

    private fun getIconGravity(push: PushNotification): Int {

        var gravity = Gravity.NO_GRAVITY
        val position: String = push.messagePosition ?: ""

        if (position.contains("top")) {
            gravity = gravity or Gravity.TOP
        }
        if (position.contains("left")) {
            gravity = gravity or Gravity.LEFT
        }
        if (position.contains("center") || push.isMegaNova()) {
            gravity = gravity or Gravity.CENTER
        }

        return gravity
    }

    private fun applyBottomGravity(push: PushNotification) {
        //if we set up gravity as Gravity.BOTTOM
        //we can`t move icon upper
        //so specify y coordinate instead of bottom gravity

        val position: String = push.messagePosition ?: ""
        if (position.contains("bottom")) {
            layoutParams?.y = screenHeight
        }
        if (position.contains("right")) {
            layoutParams?.x = screenWidth
        }
    }

    inner class  DPOverlayServiceLocalBinder : Binder() {
        fun getService() : DPOverlayService {
            return this@DPOverlayService
        }
    }

    companion object {

        private val handler = Handler(Looper.getMainLooper())

        fun start(context: Context, push: PushNotification, withText: Boolean = false) {
            val intent = Intent(context, DPOverlayService::class.java)
            intent.putExtra(KEY_EXTRA_PUSH, push.toJson())
            intent.putExtra(KEY_EXTRA_TEXT, withText)
            context.startService(intent)
        }

        fun stop(context: Context) {
            val intent = Intent(context, DPOverlayService::class.java)
            intent.putExtra(KEY_EXTRA_STOP, true)
            context.startService(intent)
        }
    }
}
