package com.m3.sdk.services.firebase

import com.m3.sdk.DynamicPush
import com.m3.sdk.data.network.log
import com.m3.sdk.utils.UserProperty
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService

 class DPInstanceIdService : FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        super.onTokenRefresh()

        val refreshedToken = FirebaseInstanceId.getInstance().token
        log("Refreshed token: $refreshedToken")
        DynamicPush.getInstance(this).setUserProperty(UserProperty.FIREBASE_TOKEN, refreshedToken)
    }
}