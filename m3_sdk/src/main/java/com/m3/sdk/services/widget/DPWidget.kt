package com.m3.sdk.services.widget

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.util.TypedValue
import android.widget.RemoteViews
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import com.m3.sdk.DynamicPush
import com.m3.sdk.R
import com.m3.sdk.models.DynamicPushEvent
import com.m3.sdk.models.EventTarget
import com.m3.sdk.models.EventType
import com.m3.sdk.utils.getDPI

class DPWidget : AppWidgetProvider() {

    override fun onEnabled(context: Context?) {
        super.onEnabled(context)

        if (context != null) {
            DPWidgetService.stop(context)
        }

        if (context != null) {
            val dpi = getDPI(context)
            val widgetManager = AppWidgetManager.getInstance(context)
            val views = RemoteViews(context.applicationContext.packageName, R.layout.view_widget_icon)
            if (dpi == 640f) {
                views.setViewPadding(R.id.textView, 0,0,0,0)
                views.setTextViewTextSize(R.id.textView, TypedValue.COMPLEX_UNIT_DIP, 16.5f)
            }
            else if (dpi == 480f) {
                views.setViewPadding(R.id.textView, 0,0,0,0)
                views.setTextViewTextSize(R.id.textView, TypedValue.COMPLEX_UNIT_DIP, 16.5f)
            } else if (dpi == 240f) {
                views.setTextViewTextSize(R.id.textView, TypedValue.COMPLEX_UNIT_DIP, 12.0f)
            } else if (dpi < 240f) {
                views.setTextViewTextSize(R.id.textView, TypedValue.COMPLEX_UNIT_DIP, 8.0f)
            }
            val ids = widgetManager.getAppWidgetIds((ComponentName(context, DPWidget::class.java)))
            widgetManager.updateAppWidget(ids, views)
        }

        DynamicPush.getInstance(context)
                .trackEvent(DynamicPushEvent()
                        .withType(EventType.SWITCH)
                        .withTarget(EventTarget.WIDGET)
                        .withValue("added"))

        Answers.getInstance()
                .logCustom(CustomEvent("Widget")
                        .putCustomAttribute("State", "added"))
    }

    override fun onDeleted(context: Context?, appWidgetIds: IntArray?) {
        super.onDeleted(context, appWidgetIds)

        DynamicPush.getInstance(context)
                .trackEvent(DynamicPushEvent()
                        .withType(EventType.SWITCH)
                        .withTarget(EventTarget.WIDGET)
                        .withValue("removed"))

        Answers.getInstance()
                .logCustom(CustomEvent("Widget")
                        .putCustomAttribute("State", "removed"))
    }
}
