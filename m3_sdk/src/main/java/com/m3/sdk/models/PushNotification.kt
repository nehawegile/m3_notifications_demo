package com.m3.sdk.models

import android.content.Context
import android.util.Log
import com.m3.sdk.services.overlay.DPOverlayService
import com.m3.sdk.services.overlay.showDefaultNotification
import com.m3.sdk.services.widget.DPWidgetService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import java.util.*

 class PushNotification(var message:         String? = null,
                       var messagePosition: String? = null,
                       var campaignType:    String? = null,
                       var displayType:     ArrayList<*>? = null,
                       var url:             String? = null,
                       var actionTitle:     String? = null,
                       var animation:       Animation? = null,
                       var _id:             String? = null,
                       var loopCount:       Int? = null,
                       var loopDelay:       Int? = null,
                       var ID:      Int = System.currentTimeMillis().toInt()) {

    class Animation(var duration:   Int? = null,
                    var size:       Int? = null,
                    var url:        String? = null,
                    var created:    Date? = null,
                    var type:       String? = null)

    fun show(context: Context) {
        if (hasAnimation()) {
            var withText = true

            if (isOverlay()) {
                DPOverlayService.start(context, this, withText)
                withText = false
            }

            if (isDPI()) {
                DPWidgetService.start(context, this, withText)
            }
        }
        else {
            showDefaultNotification(context, this)
        }
    }

    fun toJson(): String {
        return Gson().toJson(this)
    }

    fun isOverlay(): Boolean {
        return isNova() || isMegaNova() || isSuperNova()
    }

    fun isDPI(): Boolean {
        val isDPI = displayType?.contains("dpi") ?: false
        return  isDPI
    }

    fun isNova(): Boolean {
        val isNova = displayType?.contains("nova") ?: false
        return isNova
    }

    fun isSuperNova(): Boolean {
        val isSupernova = displayType?.contains("supernova") ?: false
        val isNull = displayType == null
        val isEmpty = displayType?.isEmpty() ?: true
        return isNull || isEmpty ||isSupernova
    }

    fun getNovaType(): String {
        return displayType?.find { it != "dpi" }?.toString() ?: ""
    }

    fun isMegaNova(): Boolean {
        val isMeganova = displayType?.contains("meganova") ?: false
        return isMeganova
    }

    fun hasAnimation(): Boolean {
        return animation != null && animation!!.url != null && animation!!.url!!.isNotEmpty()
    }

    fun isCloseAble(): Boolean = isNova() || isSuperNova() || isMegaNova()

    fun getDisplayDuration(): Long {
        val animationDuration: Int = animation?.duration ?: 0
        val loops: Int = loopCount ?: 0
        val delay: Int = loopDelay ?: 0

        return (animationDuration * loops + (loops - 1) * delay).toLong()
    }

    fun getLoopDelay(): Long = loopDelay?.toLong() ?: 0L

    fun getAnimationDuration(): Long = animation?.duration?.toLong() ?: 0L

    fun getDelay(): Long {
        return loopDelay?.toLong() ?: 0L
    }

    companion object {

        private val gson = Gson()

        fun from(json: String): PushNotification {
            return gson.fromJson(json, PushNotification::class.java)
        }

        fun from(remoteMessage: RemoteMessage): PushNotification {
            val pushNotification = PushNotification()
            val data = remoteMessage.data

            pushNotification._id = data["_id"]
            pushNotification.message = data["message"]
            pushNotification.messagePosition = data["messagePosition"]
            pushNotification.url = data["url"]
            pushNotification.campaignType = data["campaignType"]
            pushNotification.actionTitle = data["actionTitle"]
            pushNotification.loopCount = data["loopCount"]?.toInt()
            pushNotification.loopDelay = data["loopDelay"]?.toInt()


            val delay = pushNotification.loopDelay
            if (delay != null) {
                pushNotification.loopDelay = delay.times(1000)
            }

            val displayTypeJson = data["displayType"]
            pushNotification.displayType = gson.fromJson(displayTypeJson, ArrayList::class.java)

            val animJson = data["animation"]

            pushNotification.animation = gson.fromJson(animJson, Animation::class.java)

            Log.d("hello guys", pushNotification.animation?.url!!)
            Log.d("hello guys_url", ""+pushNotification.url)

            return pushNotification
        }
    }
}