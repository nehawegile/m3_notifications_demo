package com.m3.sdk.models

object EventType {
    val OPEN = "open"
    val CLOSE = "close"
    val CLICK = "click"
    val RECEIVE = "receive"
    val SWITCH = "switch"
    val ALIVE = "alive"
}

object EventTarget {
    val WIDGET = "widget"
    val OVERLAY = "overlay"
    val CAMPAIGN_DATA = "campaign_data"
    val NOTIFICAION = "notification"
    val BUY_BUTTON = "buy_button"
}

data class DynamicPushEvent(var eventType: String = "",
                            var eventTarget: String = "",
                            var eventValue: String? = "") {

    fun withType(type: String): DynamicPushEvent {
        eventType = type
        return this
    }

    fun withTarget(target: String): DynamicPushEvent {
        eventTarget = target
        return this
    }

    fun withValue(value: Any?): DynamicPushEvent {
        eventValue = value?.toString()
        return this
    }
}
