package com.m3.sdk

import android.content.Context
import android.content.Intent
import android.location.Location
import android.util.Log
import com.m3.sdk.callbacks.AuthorizationCallback
import com.m3.sdk.data.network.*
import com.m3.sdk.models.DynamicPushEvent
import com.m3.sdk.models.PushNotification
import com.m3.sdk.utils.*
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import java.util.*

object VerifyMethod {
    val EMAIL = "email"
    val SMS = "sms"
}

 class DynamicPushImpl(val context: Context) {

    private var userProperties: HashMap<String, String>
     private var userLocationUpdatedListner: OnLocationUpdateListener? = null
    var defaultAppIntent: Intent? = null

    init {
        userProperties = DynamicPushConfig.getInstance(context).getUserProperties()
    }

    fun authorize(apiKey: String,
                  apiSecret: String,
                  callback: AuthorizationCallback) {

        val authBody = NetworkHelper.AuthBody(
                apiKey = apiKey,
                apiSecret = apiSecret,
                uuid = getDeviceID(context))

        val isLoggedOut = isLoggedOut()

        ApiService.getInstance().auth(authBody).enqueue(NetworkHelper.AuthCallback(
                onSuccess = { response ->
                    if (response != null && response.success) {
                        if (isLoggedOut) {
                            callback.onUserNotVerified()
                        } else {
                            val userCredentials = DynamicPushConfig.UserCredentials(response.userId, response.token)
                            DynamicPushConfig.getInstance(context).saveUserCredentials(userCredentials)
                            zeroConf {
                                callback.onSuccess()
                            }

                            DynamicPush.getInstance(context).trackEvent(DynamicPushEvent()
                                    .withType("open")
                                    .withTarget("app"))
                        }
                    }
                    else {
                        callback.onError(null)
                    }
                },
                onError = { error ->
                    callback.onError(error?.message)
                },
                onUserNotVerified = {
                    callback.onUserNotVerified()
                }
        ))
    }

     private fun isLoggedOut(): Boolean = DynamicPushConfig.getInstance(context).isLoggedOut()

     fun logOut() {
         val userCredentials = DynamicPushConfig.UserCredentials("", "")
         DynamicPushConfig.getInstance(context).saveUserCredentials(userCredentials)
         DynamicPushConfig.getInstance(context).setLoggedOut(true)
     }

     fun sendVerificationCode(email: String?,
                              phone: String?,
                              onSuccess: () -> Unit,
                              onError: (String?) -> Unit) {

         val uuid = getDeviceID(context)
         var method: String? = null
         if (email != null) {
             method = VerifyMethod.EMAIL

             setUserProperty(UserProperty.EMAIL, email)

         }
         else if (phone != null) {
             method = VerifyMethod.SMS
             setUserProperty(UserProperty.PHONE, phone)
         }
         ApiService.getInstance().sendVerificationCode(uuid, method, email, phone).enqueue(NetworkHelper.RetrofitCallback<Any>({ any ->
             onSuccess.invoke()
         }, { error ->
             onError.invoke(error?.message)
         }))
     }

     fun verifyCode(code: String,
                    onSuccess: () -> Unit,
                    onError: (String?) -> Unit) {

         ApiService.getInstance().verifyCode(getDeviceID(context), code).enqueue(NetworkHelper.RetrofitCallback<Any>({ any ->
             DynamicPushConfig.getInstance(context).setLoggedOut(false)
             onSuccess.invoke()
         }, { error ->
             onError.invoke(error?.message)
         }))
     }

    fun trackEvent(event: DynamicPushEvent) {
        val credentials = DynamicPushConfig.getInstance(context).getUserCredentials()
        ApiService.getInstance().trackEvent(credentials.id, credentials.token, event).enqueue(NetworkHelper.NoCallback())
    }

    fun getUnreadNotifications(onSuccess: (list: ArrayList<PushNotification>) -> Unit,
                               onError: (String?) -> Unit) {

        val credentials = DynamicPushConfig.getInstance(context).getUserCredentials()
         ApiService.getInstance().getUnreadNotifications(credentials.id, credentials.token, 100.toDouble()).enqueue(NetworkHelper.RetrofitCallback<ArrayList<PushNotification>>({ list ->
            if (list != null) {
                onSuccess.invoke(list)
            }
            else {
                onError.invoke("List is null")
            }
        }, { error ->
            onError.invoke(error?.message)
        }))
    }

    fun setRead(notificationId: String) {
        val credentials = DynamicPushConfig.getInstance(context).getUserCredentials()
        ApiService.getInstance().setRead(credentials.id, notificationId, credentials.token).enqueue(NetworkHelper.NoCallback())
    }

    private fun zeroConf(callback: () -> Unit = {}) {
        val zeroConfProperties = HashMap<String, String>()
        zeroConfProperties.putAll(userProperties)

        val firebaseToken = FirebaseInstanceId.getInstance().token
        if (firebaseToken != null) {
            zeroConfProperties.put(UserProperty.FIREBASE_TOKEN, firebaseToken)
        }

        setUserProperties(zeroConfProperties, { restoredProperties ->
            val properties = getProperties(restoredProperties)
            userProperties.putAll(properties)
            DynamicPushConfig.getInstance(context).saveUserProperties(userProperties)
            callback()
        })
    }

    fun setUserProperty(key: String, value: Any?) {
        if (value != null) {
            setUserProperties(hashMapOf(Pair(key, value.toString())))
        }
    }

    fun setUserProperties(properties: Map<String, String>) {
        userProperties.putAll(properties)
        DynamicPushConfig.getInstance(context).saveUserProperties(userProperties)
        setUserProperties(properties, { /* empty callback */ })
    }

    private fun getProperties(body: NetworkHelper.DeviceInfoBody): HashMap<String, String> {
        val properties = HashMap<String, String>()

        if (body.pushNotificationEnabled != null) {
            tryNoException {
                properties.put(UserProperty.PUSH_ENABLED, body.pushNotificationEnabled!!.toString())
            }
        }
        if (!body.name.isNullOrEmpty()) {
            properties.put(UserProperty.NAME, body.name!!)
        }
        if (!body.gender.isNullOrEmpty()) {
            properties.put(UserProperty.GENDER, body.gender!!)
        }
        if (!body.email.isNullOrEmpty()) {
            properties.put(UserProperty.EMAIL, body.email!!)
        }
        if (!body.phone.isNullOrEmpty()) {
            properties.put(UserProperty.PHONE, body.phone!!)
        }
        if (body.age != null) {
            tryNoException {
                properties.put(UserProperty.AGE, body.age!!.toString())
            }
        }
        if (!body.country.isNullOrEmpty()) {
            properties.put(UserProperty.COUNTRY, body.country!!)
        }
        if (!body.city.isNullOrEmpty()) {
            properties.put(UserProperty.CITY, body.city!!)
        }
        if (!body.address.isNullOrEmpty()) {
            properties.put(UserProperty.STREET, body.address!!)
        }
        if (!body.zipcode.isNullOrEmpty()) {
            properties.put(UserProperty.ZIP_CODE, body.zipcode!!)
        }
        if (!body.language.isNullOrEmpty()) {
            properties.put(UserProperty.LANGUAGE, body.language!!)
        }
        if (!body.state.isNullOrEmpty()) {
            properties.put(UserProperty.STATE, body.state!!)
        }
        if (body.location != null) {
            if (body.location!!.lat != null) {
                tryNoException {
                    properties.put(UserProperty.LATITUDE, body.location!!.lat!!.toString())
                }
           }
            if (body.location!!.lng != null) {
                tryNoException {
                    properties.put(UserProperty.LONGITUDE, body.location!!.lng!!.toString())
                }
            }
        }

        return properties
    }

    private fun setUserProperties(properties: Map<String, String>, callback: (NetworkHelper.DeviceInfoBody) -> Unit? = {}) {
        log("SetUserProperties: ${Gson().toJson(properties)}")

        val body = NetworkHelper.DeviceInfoBody()

        if (properties.containsKey(UserProperty.FIREBASE_TOKEN)) {
            body.deviceToken = properties[UserProperty.FIREBASE_TOKEN]
        }
        if (properties.containsKey(UserProperty.OS_NUMBER)) {
            body.osNumber = properties[UserProperty.OS_NUMBER]
        }
        if (properties.containsKey(UserProperty.DEVICE_VENDOR)) {
            body.deviceVendor = properties[UserProperty.DEVICE_VENDOR]
        }
        if (properties.containsKey(UserProperty.DEVICE_MODEL)) {
            body.deviceModel = properties[UserProperty.DEVICE_MODEL]
        }
        if (properties.containsKey(UserProperty.DEVICE_PLATFORM)) {
            body.devicePlatform = properties[UserProperty.DEVICE_PLATFORM]
        }
        if (properties.containsKey(UserProperty.CONNECTIVITY_TYPE)) {
            body.connectivityType = properties[UserProperty.CONNECTIVITY_TYPE]
        }
        if (properties.containsKey(UserProperty.CARRIER)) {
            body.carrier = properties[UserProperty.CARRIER]
        }
        if (properties.containsKey(UserProperty.APP_VERSION)) {
            body.appVersion = properties[UserProperty.APP_VERSION]
        }
        if (properties.containsKey(UserProperty.SDK_VERSION)) {
            body.sdkVersion = properties[UserProperty.SDK_VERSION]
        }
        if (properties.containsKey(UserProperty.PUSH_ENABLED)) {
            tryNoException {
                body.pushNotificationEnabled = properties[UserProperty.PUSH_ENABLED]!!.toBoolean()
            }
        }
        if (properties.containsKey(UserProperty.PACKAGE_NAME)) {
            body.packageName = properties[UserProperty.PACKAGE_NAME]
        }
        if (properties.containsKey(UserProperty.NAME)) {
            body.name = properties[UserProperty.NAME]
        }
        if (properties.containsKey(UserProperty.GENDER)) {
            body.gender = properties[UserProperty.GENDER]
        }
        if (properties.containsKey(UserProperty.EMAIL)) {
            body.email = properties[UserProperty.EMAIL]
        }
        if (properties.containsKey(UserProperty.PHONE)) {
            body.phone = properties[UserProperty.PHONE]
        }
        if (properties.containsKey(UserProperty.AGE)) {
            tryNoException {
                body.age = properties[UserProperty.AGE]!!.toInt()
            }
        }
        if (properties.containsKey(UserProperty.COUNTRY)) {
            body.country = properties[UserProperty.COUNTRY]
        }
        if (properties.containsKey(UserProperty.CITY)) {
            body.city = properties[UserProperty.CITY]
        }
        if (properties.containsKey(UserProperty.STREET)) {
            body.address = properties[UserProperty.STREET]
        }
        if (properties.containsKey(UserProperty.ZIP_CODE)) {
            body.zipcode = properties[UserProperty.ZIP_CODE]
        }
        if (properties.containsKey(UserProperty.LANGUAGE)) {
            body.language = properties[UserProperty.LANGUAGE]
        }
        if (properties.containsKey(UserProperty.STATE)) {
            body.state = properties[UserProperty.STATE]
        }
        if (properties.containsKey(UserProperty.LATITUDE)) {
            if (body.location == null) {
                body.location = NetworkHelper.UserLocation()
            }
            tryNoException {
                body.location!!.lat = properties[UserProperty.LATITUDE]!!.toDouble()
            }
        }
        if (properties.containsKey(UserProperty.LONGITUDE)) {
            if (body.location == null) {
                body.location = NetworkHelper.UserLocation()
            }
            tryNoException {
                body.location!!.lng = properties[UserProperty.LONGITUDE]!!.toDouble()
            }
        }

        val credentials = DynamicPushConfig.getInstance(context).getUserCredentials()
        ApiService
                .getInstance()
                .updateDeviceInfo(credentials.id, credentials.token, body)
                .enqueue(NetworkHelper.RetrofitCallback<NetworkHelper.DeviceInfoBody>({ body ->
                    if (body != null) {
                        callback.invoke(body)
                    } else {
                        callback.invoke(NetworkHelper.DeviceInfoBody())
                    }
                }, { error ->
                    callback.invoke(NetworkHelper.DeviceInfoBody())
                }))
    }

    fun getUserProperty(key: String): String? {
        if (userProperties.containsKey(key)) {
            return userProperties[key]
        }
        else {
            return null
        }
    }

    fun getUserProperties(requestProperties: Map<String, String>): Map<String, String> {
        return HashMap<String, String>()
    }

    fun isPushNotificationsEnabled(): Boolean {
        if (userProperties.containsKey(UserProperty.PUSH_ENABLED)) {
            return userProperties[UserProperty.PUSH_ENABLED]!!.toBoolean()
        }
        return true
    }

    fun setPushNotificationEnabled(enabled: Boolean) {
        setUserProperty(UserProperty.PUSH_ENABLED, enabled.toString())
    }

    fun setLocationUpdatedListener(listener : OnLocationUpdateListener) {
        userLocationUpdatedListner = listener
    }

     fun getLocationUplodatedListener() : OnLocationUpdateListener? {
         return userLocationUpdatedListner
     }

    public interface OnLocationUpdateListener {
        fun onLocationUpdated(location: Location?)
    }

    companion object {

        private var _instance: DynamicPushImpl? = null

        fun initWith(context: Context, userId: String, userToken: String, firebaseOptions: FirebaseOptions, callback: AuthorizationCallback): DynamicPushImpl {
            val firebaseApps = FirebaseApp.getApps(context)
            var hasBeenInitialized = false
            for (app : FirebaseApp in firebaseApps) {
                if (app.name.equals(FirebaseApp.DEFAULT_APP_NAME)) {
                    hasBeenInitialized = true
                }
            }
            if (!hasBeenInitialized) {
                FirebaseApp.initializeApp(context, firebaseOptions)
            }

            val zeroConfProperties = HashMap<String, String>()

            val firebaseToken = FirebaseInstanceId.getInstance().token
            if (firebaseToken != null) {
                zeroConfProperties.put(UserProperty.FIREBASE_TOKEN, firebaseToken)
            }

            zeroConfProperties.put(UserProperty.APP_VERSION, getAppVersion(context))
            zeroConfProperties.put(UserProperty.PACKAGE_NAME, getPackageName(context))
            zeroConfProperties.put(UserProperty.OS_NUMBER, getOsNumber())
            zeroConfProperties.put(UserProperty.DEVICE_VENDOR, getDeviceVendor())
            zeroConfProperties.put(UserProperty.DEVICE_MODEL, getDeviceModel())
            zeroConfProperties.put(UserProperty.DEVICE_PLATFORM, getDevicePlatform())
            zeroConfProperties.put(UserProperty.SDK_VERSION, getSdkVersion())
            zeroConfProperties.put(UserProperty.CONNECTIVITY_TYPE, getConnectivityType(context))
            zeroConfProperties.put(UserProperty.CARRIER, getCarrier(context))

            DynamicPushConfig.getInstance(context).saveUserProperties(zeroConfProperties)

            _instance = DynamicPushImpl(context)
            _instance!!.authorize(userId, userToken, callback)
            return _instance!!
        }

        fun getInstance(context: Context): DynamicPushImpl {
            if (_instance == null) {
                _instance = DynamicPushImpl(context)
            }
            return _instance!!
        }
    }
}
