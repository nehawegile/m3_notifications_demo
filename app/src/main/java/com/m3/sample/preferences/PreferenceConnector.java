package com.m3.sample.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * The class PreferenceConnector is a class useful to simplify you the
 * interaction with your app preferences. In fact it has methods that interact
 * with the basical features of SharedPreferences but still the possibility to
 * obtain preferences.
 */
public class PreferenceConnector {
    public static final String PREF_NAME = "M3_PREFERENCES";
    public static final int MODE = Context.MODE_PRIVATE;

    //Maintain last screen to move direct inside app
    public final static String PREF_SCREEN = "screenName";

    // App User
    public final static String PREF_IS_USER_LOGGEDIN = "user_looggedin";
    public final static String PREF_USER_ID = "user_id";
    public final static String PREF_USER_IMAGE = "user_image";
    public final static String PREF_USER_NAME = "user_name";
    public final static String PREF_USER_FIRST_NAME = "user_first_name";
    public final static String PREF_USER_LAST_NAME = "user_last_name";
    public final static String PREF_USER_EMAIL = "user_email";
    public final static String PREF_USER_PASSWORD = "user_password";
    public final static String PREF_USER_PHONE = "user_phone";
    public final static String PREF_USER_GENDER = "user_gender";
    public final static String PREF_USER_DOB = "user_dob";
    public final static String PREF_TOKEN = "token";
    public final static String DEVICE_TOKEN = "DEVICE_TOKEN";
    public final static String DEVICE_TOKEN_OLD = "DEVICE_TOKEN_OLD";
    public final static String PREF_ANDROID_ID = "android_id";
    public final static String PREF_PASSCODE = "passcode";
    public final static String PREF_RATING= "rating";
    public final static String PREF_SORT_BY_PRICE= "sort_by_price";
    public final static String PREF_SORT_BY_DISTANCE_RATING= "sort_by_distance_rating";
    public final static String PREF_SORT_BY_ALL= "sort_by_all";
    public final static String PREF_SORT_BY_PRODUCTS= "sort_by_products";
    public final static String PREF_SORT_BY_SERVICES= "sort_by_services";
    public final static String PREF_SORT_BY_DEALS= "sort_by_deals";


    public final static String PREF_BRAINTREE_TOKEN= "braintree_token";
    public final static String PREF_ID= "id";
    public final static String PREF_BUSINESS_CATEGORY= "business_category";
    public final static String PREF_CURRENT_LATITUDE= "current_latitude";
    public final static String PREF_CURRENT_LONGITUDE= "current_longitude";
    public final static String PREF_PUSH_NOTIFICATION= "push_notification";
    public final static String PREF_MAINSCREEN_PAGINATION_NUMBER= "mainscreen_pagination_number";
    public final static String PREF_GRID_PAGINATION_NUMBER= "grid_pagination_number";
    public final static String PREF_CURRENT_LOCATION= "current_location";
    public final static String PREF_WEBRESPONSE= "webresponse";

    public static void writeBoolean(String key, boolean value, Context context) {
        getEditor(context).putBoolean(key, value).commit();
    }

    public static boolean readBoolean(String key, boolean defValue, Context context) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    public static void writeInteger(String key, int value, Context context) {
        getEditor(context).putInt(key, value).commit();

    }

    public static int readInteger(String key, int defValue, Context context) {
        return getPreferences(context).getInt(key, defValue);
    }

    public static void writeString(String key, String value, Context context) {
        getEditor(context).putString(key, value).commit();

    }

    public static String readString(String key, String defValue, Context context) {
        return getPreferences(context).getString(key, defValue);
    }

    public static void writeFloat(String key, float value, Context context) {
        getEditor(context).putFloat(key, value).commit();
    }

    public static float readFloat(String key, float defValue, Context context) {
        return getPreferences(context).getFloat(key, defValue);
    }

    public static void writeLong(String key, long value, Context context) {
        getEditor(context).putLong(key, value).commit();
    }

    public static long readLong(String key, long defValue, Context context) {
        return getPreferences(context).getLong(key, defValue);
    }

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    public static Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

    public static void clearAll(Context context) {
        getEditor(context).clear();
        getEditor(context).commit();
    }
    public static void remove(String key,Context context)
    {
        if(PreferenceConnector.getPreferences(context).contains(key)) {
            SharedPreferences.Editor edit = getEditor(context);
            edit.remove(key);
            edit.commit();
        }
    }

    public static void logout (Context context){
        PreferenceConnector.writeString(PreferenceConnector.PREF_NAME, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_USER_FIRST_NAME, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_USER_LAST_NAME, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_USER_EMAIL, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_USER_PASSWORD, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_USER_PHONE, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_ANDROID_ID, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_PASSCODE, "", context);
        PreferenceConnector.writeBoolean(PreferenceConnector.PREF_IS_USER_LOGGEDIN, false, context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_USER_NAME, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_USER_DOB, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_TOKEN, "", context);
//        PreferenceConnector.writeString(PreferenceConnector.DEVICE_TOKEN, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_USER_ID, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_USER_IMAGE, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_RATING, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_SORT_BY_PRICE, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_SORT_BY_DISTANCE_RATING, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_SORT_BY_ALL, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_SORT_BY_PRODUCTS, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_SORT_BY_SERVICES, "", context);
        PreferenceConnector.writeString(PreferenceConnector.PREF_SORT_BY_DEALS, "", context);
    }

}
