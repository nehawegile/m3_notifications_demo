package com.m3.sample.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.m3.sample.R;
import com.m3.sample.adapters.DemoNotificationAdapter;
import com.m3.sample.preferences.PreferenceConnector;

/**
 * Created by ${User} on 27/2/18.
 */

public class MyCart extends AppCompatActivity {
    RecyclerView rv;
    DemoNotificationAdapter adapter ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycart);
        final LinearLayout ll_mainll = (LinearLayout) findViewById(R.id.ll_mainll);
        ll_mainll.setVisibility(View.GONE);
        if(PreferenceConnector.getPreferences(this).contains("product_1"))
        {
            ll_mainll.setVisibility(View.VISIBLE);
            ((ImageView)findViewById(R.id.mainimage)).setBackgroundResource(R.mipmap.bahubali);
        }
        final LinearLayout ll_mainll1 = (LinearLayout) findViewById(R.id.ll_mainll1);
        ll_mainll1.setVisibility(View.GONE);
        if(PreferenceConnector.getPreferences(this).contains("product_2"))
        {
            ll_mainll1.setVisibility(View.VISIBLE);
            ((ImageView)findViewById(R.id.mainimage1)).setBackgroundResource(R.mipmap.business_2);

        }
        if(!PreferenceConnector.getPreferences(this).contains("product_1") && !PreferenceConnector.getPreferences(this).contains("product_2"))
        {
            ((TextView)findViewById(R.id.emptyCart)).setVisibility(View.VISIBLE);
        }

        ((Button) findViewById(R.id.remove_1)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceConnector.remove("product_1",MyCart.this);
                ll_mainll.setVisibility(View.GONE);

            }
        });
        ((Button) findViewById(R.id.remove_2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceConnector.remove("product_2",MyCart.this);
                ll_mainll1.setVisibility(View.GONE);

            }
        });


    }
    public void onBackClick(View v)
    {
        onBackPressed();
    }
}
