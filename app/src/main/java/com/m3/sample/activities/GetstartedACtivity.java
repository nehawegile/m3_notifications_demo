package com.m3.sample.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.m3.sample.R;
import com.m3.sample.Util.ConnectionCheck;
import com.m3.sample.Util.Utils;

import com.m3.sample.interfaces.APIInterface;
import com.m3.sample.preferences.PreferenceConnector;
import com.m3.sample.webservice.APIClient;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetstartedACtivity extends AppCompatActivity {

    public static final int PERMISSION_ALL = 2237;
    APIInterface apiInterface;

    private TextView onguestclick;
    private ConnectionCheck mConnectionCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getstarted_activity);
        PreferenceConnector.logout(this);
        if (mConnectionCheck == null) {
            ConnectionCheck.getInstance().setContext(getApplicationContext());
            mConnectionCheck = ConnectionCheck.getInstance();
        }

        apiInterface = APIClient.getClient().create(APIInterface.class);
        initview();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ALL:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                break;
        }
    }

    void initview() {

        onguestclick = (TextView) findViewById(R.id.onguestclick);
        String term = new String(getString(R.string.term));
        SpannableString termcontent = new SpannableString(term);
        termcontent.setSpan(new UnderlineSpan(), 0, term.length(), 0);

        String policy = new String(getString(R.string.policy));
        SpannableString policycontent = new SpannableString(policy);
        policycontent.setSpan(new UnderlineSpan(), 0, policy.length(), 0);

        TextView termtext = (TextView) findViewById(R.id.termstextview);
        termtext.setText(termcontent);
        TextView policytext = (TextView) findViewById(R.id.privacypolicytetview);
        policytext.setText(policycontent);
    }





    public void onclickguest(View view) {

//        onguestclick.setBackgroundResource(R.drawable.white_border);
        onguestclick.setPressed(true);
        Intent intent = new Intent(this, ExplainerActivity.class);
        startActivity(intent);
        Utils.overidependingtrasition(this);
    }

    public void onclicksignin(View view) {

//        onclicksignup.setPressed(true);
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        Utils.overidependingtrasition(this);


    }

    public void onclicksignup(View view) {
//        onclicksignup.setPressed(true);
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
        Utils.overidependingtrasition(this);
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}
