package com.m3.sample.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.m3.sample.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    Double Latitude=0.0;
    Double Longitude=0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        if (intent.hasExtra("Latitude")) {
            Latitude = intent.getDoubleExtra("Latitude",Latitude);
        }
        if (intent.hasExtra("Longitude")) {
            Longitude = intent.getDoubleExtra("Longitude",Longitude);
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        if (Latitude!=0.0&&Longitude!=0.0) {
            LatLng eventposition = new LatLng(Latitude, Longitude);
            mMap.addMarker(new MarkerOptions().position(eventposition).title("Headmasters"));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(eventposition, 12.0f));
        }
    }

    @OnClick(R.id.get_directions)
    void onGetDirectionsCLickButton() {
        startNavigationActivity(Latitude, Longitude, this);
    }

    public static void startNavigationActivity(Double UserLat,Double UsersLong, Context activity) {
        activity.startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse(String.format("google.navigation:q=" + UserLat + "," + UsersLong))));
    }

    public void onBackClick(View view) {
        onBackPressed();
    }
}
