package com.m3.sample.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.m3.sample.R;
import com.m3.sample.Util.Utils;
import com.m3.sample.fragments.Explaner_threeFragment;
import com.m3.sample.fragments.Explaner_twoFragment;
import com.m3.sample.fragments.SplashScreenPager;
import com.viewpagerindicator.CirclePageIndicator;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class ExplainerActivity extends AppCompatActivity {

    private static final int UNABLE_PLAY_SERVICES = 1;
    private static final int REQUEST_LOCATION_PERMITION = 2;
    private ViewPager coach_mark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explainer);
        initview();
    }

    void initview() {
        CirclePageIndicator circular_indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        coach_mark = (ViewPager) findViewById(R.id.viewpager);
        coach_mark.setOffscreenPageLimit(3);
        coach_mark.setAdapter(new CoachMarkAdapter(getSupportFragmentManager()));
        circular_indicator.setViewPager(coach_mark);
    }

    public void onclickselectcity(View view) {
        Intent intent = new Intent(this, SelectCityActivity.class);
        startActivity(intent);
    }

    public void onclicknearby(View view) {
//        getlocation();
        Intent intent = new Intent(ExplainerActivity.this, DemoHomeActivity.class).putExtra("isFromGuest",true);
        startActivity(intent);
    }







    class CoachMarkAdapter extends FragmentPagerAdapter {

        public CoachMarkAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 3;
        }


        @Override
        public Fragment getItem(int position) {
            switch (position) {

                case 0:
                    return new SplashScreenPager();
                case 1:
                    return new Explaner_twoFragment();
                case 2:
                    return new Explaner_threeFragment();


            }

            return null;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}
