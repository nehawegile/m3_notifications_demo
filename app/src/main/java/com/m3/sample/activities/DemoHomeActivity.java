package com.m3.sample.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.m3.sample.R;
import com.m3.sample.Util.Utils;
import com.m3.sample.preferences.PreferenceConnector;

/**
 * Created by ${User} on 22/2/18.
 */

public class DemoHomeActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ((LinearLayout)findViewById(R.id.ll_mainll)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DemoHomeActivity.this,DemoDetailActivity.class).putExtra("product",1));
            }
        });
        ((LinearLayout)findViewById(R.id.ll_mainll1)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DemoHomeActivity.this,DemoDetailActivity.class).putExtra("product",2));
            }
        });
    }
    public void onclickclosedrawer(View view) {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }

    }

    public void onclickmenubutton(View view) {
        Utils.hideKeyboard(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.openDrawer(GravityCompat.START);

    }

    void closedrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }


    public void onclickmyprofile(View view) {
        closedrawer();
        Intent intent = new Intent(this, MyProfileActivity.class);
        startActivity(intent);
    }

    public void onclicknotifications_messages(View view) {

        Intent newIntnt = getIntent();
        if(!newIntnt.getBooleanExtra("isFromGuest",false)) {

            startActivity(new Intent(this, DemoNotification.class).putExtra("email", newIntnt.getStringExtra("email"))
                    .putExtra("name", newIntnt.getStringExtra("name")));
        }
        else
            Toast.makeText(this,"Please login first!",Toast.LENGTH_SHORT).show();
            closedrawer();
    }

    public void onclicknewdeals(View view) {
//        String client_id = PreferenceConnector.readString(PreferenceConnector.PREF_USER_ID, "", DemoHomeActivity.this);
//
//        if(!client_id.equals("")) {
//            Intent intent = new Intent(this, SettingsActivity.class);
//            startActivity(intent);
//            Utils.overidependingtrasition(this);
            closedrawer();
//        } else{
////            Utils.loginDailog(DemoHomeActivity.this);
//        }
    }

    public void onclickmycart(View view) {
//        String client_id = PreferenceConnector.readString(PreferenceConnector.PREF_USER_ID, "", DemoHomeActivity.this);
//
//        if(!client_id.equals("")) {
            Intent intent = new Intent(this, MyCart.class);
            startActivity(intent);
////            Utils.overidependingtrasition(this);
//        } else{
//            Utils.loginDailog(DemoHomeActivity.this);
//        }
        closedrawer();
    }



    public void onclicksignout(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.are_you_sure_to_signout);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                PreferenceConnector.getPreferences(DemoHomeActivity.this);
                PreferenceConnector.clearAll(DemoHomeActivity.this);
                finish();
                Intent intent=new Intent(DemoHomeActivity.this,GetstartedACtivity.class);
                startActivity(intent);
                finish();
                ActivityCompat.finishAffinity(DemoHomeActivity.this);
                Utils.overidependingtrasition(DemoHomeActivity.this);
            }
        })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alert11 = builder.create();
        alert11.show();

    }
}
