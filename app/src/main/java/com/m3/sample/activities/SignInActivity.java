package com.m3.sample.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.firebase.FirebaseOptions;
import com.google.gson.Gson;
import com.m3.sample.BuildConfig;
import com.m3.sample.R;
import com.m3.sample.Util.ConnectionCheck;
import com.m3.sample.Util.Utils;
import com.m3.sample.interfaces.APIInterface;
import com.m3.sample.models.SignInResponse;
import com.m3.sample.preferences.PreferenceConnector;
import com.m3.sdk.DynamicPush;
import com.m3.sdk.callbacks.AuthorizationCallback;
import com.m3.sample.webservice.APIClient;

import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity {

    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.password)
    TextView password;
    private HashMap<String,String> mUserProfile;
    APIInterface apiInterface;
    String android_id ;
    private ConnectionCheck mConnectionCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in2);
        ButterKnife.bind(this);

        mUserProfile = new HashMap<>();

        if (mConnectionCheck == null) {
            ConnectionCheck.getInstance().setContext(getApplicationContext());
            mConnectionCheck = ConnectionCheck.getInstance();
        }
        android_id =  PreferenceConnector.readString(PreferenceConnector.PREF_ANDROID_ID, "", this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    public  void onclickback(View view){
        onBackPressed();
    }


    private void saveUserProperties() {
        if (mUserProfile.size()>0) {
            DynamicPush.getInstance(this).setUserProperties(mUserProfile);
        }

    }

    private void authenticateApp() {
        String apiKey = BuildConfig.apiKey;
        String apiSecretKey = BuildConfig.apiSecret;

//DEV
//        FirebaseOptions firebaseOptions = new FirebaseOptions.Builder().setApiKey("AIzaSyBRafMfa1q7s3hROsEIB1-5ip0LT_dmCmo").setApplicationId("1:406620688930:android:970f588ec1f1d738").setGcmSenderId("406620688930").build();
//STAGE
        FirebaseOptions firebaseOptions = new FirebaseOptions.Builder().setApiKey("AIzaSyBpkDZb8Z6bIeUYlUNUE7Rc1RE9UFL3cGA").setApplicationId("1:970235594675:android:7c4e4ca161caff8d").setGcmSenderId("970235594675").build();

        DynamicPush.initWith(this, apiKey, apiSecretKey, firebaseOptions, new AuthorizationCallback() {
            @Override
            public void onUserNotVerified() {
                Utils.dismissprogressdialog(SignInActivity.this);
                Toast.makeText(SignInActivity.this, "User is not verified !", Toast.LENGTH_LONG).show();
            }
            @Override
            public void onError(@Nullable String reason) {
                Utils.dismissprogressdialog(SignInActivity.this);
                Toast.makeText(SignInActivity.this, "Error!", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onSuccess() {
                Utils.dismissprogressdialog(SignInActivity.this);
                PreferenceConnector.writeBoolean(PreferenceConnector.PREF_IS_USER_LOGGEDIN, true, SignInActivity.this);

                Toast.makeText(SignInActivity.this, "Welcome to M3 !", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SignInActivity.this, DemoHomeActivity.class);
                intent.putExtra("email",mUserProfile.get("email"));
                intent.putExtra("name",mUserProfile.get("name"));
                PreferenceConnector.getPreferences(SignInActivity.this);
                PreferenceConnector.writeString("email",mUserProfile.get("email"),SignInActivity.this);
                PreferenceConnector.writeString("name",mUserProfile.get("name"),SignInActivity.this);

                startActivity(intent);
                Utils.overidependingtrasition(SignInActivity.this);
                ActivityCompat.finishAffinity(SignInActivity.this);
            }
        });
    }

    public void onclicksignin(){
        String passwrd=password.getText().toString().trim();
        Boolean isValidPassword=passwrd.contains(" ");
        String emailId=email.getText().toString();
        boolean isvalid = Utils.isValidEmail(emailId);
        if(isvalid == true){
            PreferenceConnector.writeString(PreferenceConnector.PREF_USER_EMAIL, emailId, this);
        }

        if (TextUtils.isEmpty(passwrd) || TextUtils.isEmpty(emailId) || isvalid==false ||isValidPassword==true ) {
            if (TextUtils.isEmpty(emailId)) {
                Utils.callSnackbar(this, getString(R.string.please_enter_email));
            }
            else if (isvalid==false){
                Utils.callSnackbar(this,  getString(R.string.please_enter_valid_email));
            }
            else if (TextUtils.isEmpty(passwrd) ) {
                Utils.callSnackbar(this, getString(R.string.please_enter_password));
            }
            else if(isValidPassword==true){
                Utils.callSnackbar(this, getString(R.string.password_not_contain_space));
            }
        }else if (mConnectionCheck.isNetworkConnected()) {
            JSONObject jsonObject = new JSONObject();
            try {
                PreferenceConnector.writeString(PreferenceConnector.PREF_USER_PASSWORD, passwrd, SignInActivity.this);
                jsonObject.accumulate("email", emailId);
                jsonObject.accumulate("password", passwrd);
                jsonObject.accumulate("device_token", PreferenceConnector.readString(PreferenceConnector.DEVICE_TOKEN, "", this));
                jsonObject.accumulate("device_id", android_id);
                jsonObject.accumulate("device_type", "A");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //TODO COMMENT
            mUserProfile.put("name","Sam");
            mUserProfile.put("email",emailId);

            saveUserProperties();
            authenticateApp();
//            RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
//
//            Utils.progressdialog(SignInActivity.this);
//            Call call = apiInterface.signIn(body);

//            call.enqueue(new Callback<ResponseBody>() {
//
//                @Override
//                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    Log.d("TAG",response.code()+"");
//                    Utils.dismissprogressdialog(SignInActivity.this);
//                    SignInResponse signInResponse=null;
//                    try {
//                        signInResponse = new Gson().fromJson(response.body().string(), SignInResponse.class);
//                        if (null != signInResponse && signInResponse.getError()==false) {
//                            String phone = signInResponse.getUserInfo().getPhone();
//                            String user_id = signInResponse.getUserInfo().getId();
//                            String user_email = signInResponse.getUserInfo().getEmail();
//                            String user_firstname = signInResponse.getUserInfo().getFirstname();
//                            String user_lastname = signInResponse.getUserInfo().getLastname();
//                            String token = signInResponse.getToken();
//                            String imageName= signInResponse.getUserInfo().getImage();
//                            String finalDate = null;
//                            if(signInResponse.getUserInfo().getBirthday()!=null) {
//                                String dob = signInResponse.getUserInfo().getBirthday();
//                                if (dob != null) {
//                                    String dateparts[] = dob.split("T");
//                                    finalDate = dateparts[0];
//                                }
//                            }
//
//                            PreferenceConnector.writeString(PreferenceConnector.PREF_USER_PHONE, phone, SignInActivity.this);
//                            PreferenceConnector.writeString(PreferenceConnector.PREF_USER_ID, user_id, SignInActivity.this);
//                            PreferenceConnector.writeString(PreferenceConnector.PREF_USER_EMAIL, user_email, SignInActivity.this);
//                            PreferenceConnector.writeString(PreferenceConnector.PREF_USER_FIRST_NAME, user_firstname, SignInActivity.this);
//                            PreferenceConnector.writeString(PreferenceConnector.PREF_USER_LAST_NAME, user_lastname, SignInActivity.this);
//                            PreferenceConnector.writeString(PreferenceConnector.PREF_TOKEN, token, SignInActivity.this);
//                            PreferenceConnector.writeBoolean(PreferenceConnector.PREF_IS_USER_LOGGEDIN, true, SignInActivity.this);
//                            PreferenceConnector.writeString(PreferenceConnector.PREF_USER_DOB, finalDate, SignInActivity.this);
//                            PreferenceConnector.writeString(PreferenceConnector.PREF_USER_IMAGE, imageName, SignInActivity.this);
//
//                            gotoHomeActivity();
//                        } else {
//                            Utils.callSnackbar(SignInActivity.this, signInResponse.getMessage());
//                        }
//                    } catch (Exception e){
//
//                    }
//                }
//
//                @Override
//                public void onFailure(Call call, Throwable t) {
//                    Utils.dismissprogressdialog(SignInActivity.this);
//                    Log.e("onFailure: ", "Fail" );
//                    Utils.callSnackbar(SignInActivity.this, "Failed to connect to sever");
//
//                }
//            });
        }
        else
            Utils.callSnackbar(this, getString(R.string.noInternet));
    }

    @OnClick({R.id.signInBTn})
    public void gotoMainActivity(View v) {
        switch (v.getId()) {
            case R.id.signInBTn:
                onclicksignin();
                break;
        }
    }
}
