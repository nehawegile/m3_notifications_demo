package com.m3.sample.activities;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.m3.sample.BuildConfig;
import com.m3.sample.R;
import com.m3.sdk.data.network.ApiService;

import io.fabric.sdk.android.Fabric;


/**
 * Created by Bharat on 11/7/16.
 */
public class M3ApplicationClass extends Application {

    public static M3ApplicationClass APP;
    public static Typeface FontTypefaceArialRegular;
    public static Typeface FontTypefaceArialRegularbold;


    public M3ApplicationClass() {

        APP = this;
        ApiService.Companion.setBaseUrl(BuildConfig.url);
    }


    public static M3ApplicationClass getInstance() {

        if (APP == null) {
            throw new IllegalStateException();
        }

        return APP;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FontTypefaceArialRegular = Typeface.createFromAsset(
                getApplicationContext().getAssets(), getString(R.string.font_family_arial_name));
        FontTypefaceArialRegularbold = Typeface.createFromAsset(
                getApplicationContext().getAssets(), getString(R.string.font_family_arial_name_bold));

        Fabric.with(this, new Crashlytics());

        ApiService.Companion.setBaseUrl(BuildConfig.url);

    }


}
