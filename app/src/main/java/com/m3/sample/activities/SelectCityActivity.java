package com.m3.sample.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.m3.sample.R;
import com.m3.sample.Util.Utils;
import com.m3.sample.adapters.Countryadapter;
import com.m3.sample.interfaces.Countryadapterlistener;
import com.m3.sample.models.CitiesGetterSetter;

import java.util.ArrayList;

public class SelectCityActivity extends AppCompatActivity implements Countryadapterlistener,AdapterView.OnItemClickListener {
    String[] cities = {"All Cities", "New York", "Chandigarh", "Delhi", "London", "Mumbai", "Chandigarh", "Delhi", "London", "Mumbai","Bangalore"};
    ArrayList<CitiesGetterSetter> citylist = new ArrayList<>();

    private ImageView tickicon;
    private EditText autedittext;
    private RelativeLayout listlayout;
    private ListView listview;
    boolean iscityviewopen = false;
    private Countryadapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_city);
        initview();
    }

    void initview() {
        setvalues();
        autedittext = (EditText) findViewById(R.id.autoedittext);
        tickicon = (ImageView) findViewById(R.id.tickicon);
        listlayout = (RelativeLayout) findViewById(R.id.listlayout);
        listview = (ListView) findViewById(R.id.listview);
        listview.setOnItemClickListener(this);

        autedittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                autedittext.setCursorVisible(true);
            }
        });

        setcountryadapter();


    }


    void setcountryadapter() {
        adapter = new Countryadapter(SelectCityActivity.this, citylist,this);
        listview.setAdapter(adapter);
        autedittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                listview.smoothScrollToPosition(0);

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                String stringafterchange = editable.toString();

                if (stringafterchange.trim().length() > 0) {
                    adapter.filter(stringafterchange);
                    listview.smoothScrollToPosition(0);
                    tickicon.setVisibility(View.VISIBLE);
                    tickicon.setImageResource(R.drawable.cancel_text);
                    listlayout.setVisibility(View.VISIBLE);
                    iscityviewopen = true;
                } else {
                    listview.smoothScrollToPosition(0);
                    tickicon.setVisibility(View.GONE);
                    tickicon.setImageResource(R.drawable.tickicon);
                    listlayout.setVisibility(View.GONE);
                    iscityviewopen = false;
                }
            }
        });
    }

    public void onclicktickcancel(View view) {

        if (iscityviewopen) {
            listview.smoothScrollToPosition(0);
            iscityviewopen = false;
            tickicon.setVisibility(View.GONE);
            listlayout.setVisibility(View.GONE);
            autedittext.setText("");
        }


    }

    @Override
    public void onBackPressed() {

        if (iscityviewopen) {
            listview.smoothScrollToPosition(0);
            listlayout.setVisibility(View.GONE);
            iscityviewopen = false;
        } else
            super.onBackPressed();
    }

    public void setvalues() {
        for (int i = 0; i < cities.length; i++) {

            CitiesGetterSetter citiesGetterSetter = new CitiesGetterSetter();
            citiesGetterSetter.setCity(cities[i]);
            citylist.add(citiesGetterSetter);

        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        autedittext.setText(citylist.get(i).getCity());
        autedittext.setCursorVisible(false);
        iscityviewopen = false;
        tickicon.setImageResource(R.drawable.tickicon);
        tickicon.setVisibility(View.VISIBLE);
        listview.smoothScrollToPosition(0);
        listlayout.setVisibility(View.GONE);
        Utils.hideKeyboard(this);
        Utils.progressdialog(this);
        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Utils.dismissprogressdialog(SelectCityActivity.this);
                Intent intent=new Intent(SelectCityActivity.this,DemoHomeActivity.class).putExtra("isFromGuest",true);
                startActivity(intent);
                finish();
            }
        },1500);

    }


    @Override
    public void onchangestate(boolean state, int position) {

        if(state) {
            autedittext.setText(citylist.get(position).getCity());
            autedittext.setCursorVisible(false);
            iscityviewopen = false;
            tickicon.setImageResource(R.drawable.tickicon);
            tickicon.setVisibility(View.VISIBLE);
            listview.smoothScrollToPosition(0);
            listlayout.setVisibility(View.GONE);

            Utils.hideKeyboard(this);
            Utils.progressdialog(this);
            Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Utils.dismissprogressdialog(SelectCityActivity.this);
                    Intent intent=new Intent(SelectCityActivity.this,DemoHomeActivity.class).putExtra("isFromGuest",true);
                    startActivity(intent);
                    finish();
                }
            },1500);

        }
    }
}
