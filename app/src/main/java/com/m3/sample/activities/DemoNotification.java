package com.m3.sample.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.FirebaseOptions;
import com.m3.sample.BuildConfig;
import com.m3.sample.R;
import com.m3.sample.Util.Utils;
import com.m3.sample.adapters.DemoNotificationAdapter;
import com.m3.sample.preferences.PreferenceConnector;
import com.m3.sdk.DynamicPush;
import com.m3.sdk.callbacks.AuthorizationCallback;
import com.m3.sdk.models.DynamicPushEvent;
import com.m3.sdk.models.PushNotification;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;

/**
 * Created by ${User} on 22/2/18.
 */

public class DemoNotification extends AppCompatActivity {
    RecyclerView rv;
    DemoNotificationAdapter adapter ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_notification);
//        text = (TextView) findViewById(R.id.notification_text);

        rv = findViewById(R.id.rv_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.supportsPredictiveItemAnimations();
        rv.setLayoutManager(linearLayoutManager);
//        mUserProfile.put("email",PreferenceConnector.readString("email","",DemoNotification.this));
//        mUserProfile.put("name",PreferenceConnector.readString("name","",DemoNotification.this));
//        M3Solutions.getInstance(this).setUserProperties(mUserProfile);
//        authenticateApp();
        getNotification();
    }
    private void getNotification()
    {
        DynamicPush.getInstance(this).trackEvent(new DynamicPushEvent()
                .withType("screen")
                .withTarget("dps")
                .withValue("opened"));
        DynamicPush.getInstance(this).getUnreadNotifications(
                new Function1<ArrayList<PushNotification>, Unit>() {
                    @Override
                    public Unit invoke(ArrayList<PushNotification> pushNotifications) {
                        if(pushNotifications.size()>0) {

                            adapter = new DemoNotificationAdapter(DemoNotification.this,pushNotifications);
                            rv.setAdapter(adapter);
//                            text.setText(pushNotifications.get(0).getMessage() + pushNotifications.get(0).getUrl());
                        }
                        return null;
                    }
                },
                new Function1<String, Unit>() {
                    @Override
                    public Unit invoke(String s) {
                        return null;
                    }
                }
        );
    }

    public void onBackClick(View view) {
        onBackPressed();
    }
}
