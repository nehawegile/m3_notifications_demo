package com.m3.sample.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import com.m3.sample.R;
import com.m3.sample.Util.Utils;
import com.m3.sample.preferences.PreferenceConnector;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        Utils.printHashKey(this);


//        Handler handler=new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                Intent intent=new Intent(SplashActivity.this,GetstartedACtivity.class);
//                startActivity(intent);
//                Utils.overidependingtrasition(SplashActivity.this);
//            }
//        },1500);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent dataIntent = getIntent();
                if(dataIntent!=null && dataIntent.hasExtra("product_1"))
                {
                    startActivity(new Intent(SplashActivity.this,DemoDetailActivity.class).putExtra("product_1","1"));
                }
                else
                if (!PreferenceConnector.readBoolean(PreferenceConnector.PREF_IS_USER_LOGGEDIN, false, SplashActivity.this)) {
                    Intent intent=new Intent(SplashActivity.this,GetstartedACtivity.class);
                    startActivity(intent);
                    Utils.overidependingtrasition(SplashActivity.this);
                } else {
                    Intent intent=new Intent(SplashActivity.this,DemoHomeActivity.class);
                    startActivity(intent);
                    Utils.overidependingtrasition(SplashActivity.this);
                }
                ActivityCompat.finishAffinity(SplashActivity.this);
            }
        }, 1500);
    }
}
