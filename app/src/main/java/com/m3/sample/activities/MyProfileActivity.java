package com.m3.sample.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.m3.sample.R;
import com.m3.sample.Util.ConnectionCheck;
import com.m3.sample.Util.Utils;
import com.m3.sample.interfaces.APIInterface;
import com.m3.sample.preferences.PreferenceConnector;
import com.m3.sample.webservice.APIClient;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyProfileActivity extends AppCompatActivity {

    private ConnectionCheck mConnectionCheck;
    APIInterface apiInterface;
    private int MY_PROFILE_REQUEST = 125;
    String PREF_USER_FIRST_NAME;
    String PREF_USER_LAST_NAME;
    String PREF_USER_EMAIL;
    String PREF_USER_IMAGE;
    private BroadcastReceiver mMessageReceiver = null;
    String message;


    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email_id)
    TextView email_id;
    @BindView(R.id.userImage)
    ImageView userImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        ButterKnife.bind(this);

        mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("message")) {
                    message = intent.getStringExtra("message");
                    Utils.callSnackbar(MyProfileActivity.this, message);
                }
            }
        };

        if (mConnectionCheck == null) {
            ConnectionCheck.getInstance().setContext(getApplicationContext());
            mConnectionCheck = ConnectionCheck.getInstance();
        }
        apiInterface = APIClient.getClient().create(APIInterface.class);
        PreferenceConnector.readString(PreferenceConnector.DEVICE_TOKEN, "", this);
        initview();

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("EVENT_SNACKBAR"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    private void initview() {

        PREF_USER_FIRST_NAME = PreferenceConnector.readString(PreferenceConnector.PREF_USER_FIRST_NAME, "", MyProfileActivity.this);
        PREF_USER_LAST_NAME=PreferenceConnector.readString(PreferenceConnector.PREF_USER_LAST_NAME, "", MyProfileActivity.this);
        PREF_USER_EMAIL=PreferenceConnector.readString(PreferenceConnector.PREF_USER_EMAIL, "", MyProfileActivity.this);
        PREF_USER_IMAGE=PreferenceConnector.readString(PreferenceConnector.PREF_USER_IMAGE, "", MyProfileActivity.this);

        String fullName=PREF_USER_FIRST_NAME+ " " + PREF_USER_LAST_NAME;
        String caps_name = String.valueOf(fullName.charAt(0)).toUpperCase() + fullName.subSequence(1, fullName.length());
        name.setText(caps_name);

        email_id.setText(PREF_USER_EMAIL);

      userImage.setImageResource(R.mipmap.my_profile);

        /*if (mConnectionCheck.isNetworkConnected()) {
            JSONObject jsonObject = new JSONObject();
            try {
                String client_id = PreferenceConnector.readString(PreferenceConnector.PREF_USER_ID, "", this);
                String PREF_TOKEN = PreferenceConnector.readString(PreferenceConnector.PREF_TOKEN, "", this);
                String android_id =  PreferenceConnector.readString(PreferenceConnector.PREF_ANDROID_ID, "", this);

                jsonObject.accumulate("client_id", client_id);
                jsonObject.accumulate("source_id", client_id);
                jsonObject.accumulate("source", "client");
                jsonObject.accumulate("session_key", PREF_TOKEN);
                jsonObject.accumulate("device_id", android_id);
                jsonObject.accumulate("device_type", "A");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());

            Utils.progressdialog(MyProfileActivity.this);
            Call call = apiInterface.getProfileInfo(body);

            call.enqueue(new Callback<ResponseBody>() {

                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Log.d("TAG", response.code() + "");
                    Utils.dismissprogressdialog(MyProfileActivity.this);
                    MyProfileResponse myProfileResponse = null;
                    try {
                        myProfileResponse = new Gson().fromJson(response.body().string(), MyProfileResponse.class);
                        if (null != myProfileResponse && myProfileResponse.getError() == false) {
                            PreferenceConnector.writeString(PreferenceConnector.PREF_USER_NAME, "", MyProfileActivity.this);
                            PreferenceConnector.writeString(PreferenceConnector.PREF_USER_EMAIL, "", MyProfileActivity.this);

                            //finish();
                            //gotoHomeActivity();
                        } else {
                            //Utils.callSnackbar(MyProfileActivity.this, myProfileResponse.getMessage());
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Utils.dismissprogressdialog(MyProfileActivity.this);
                    Log.e("onFailure: ", "Fail");
                }
            });
        }*/

    }



    public  void onclicksignout(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.are_you_sure_to_signout);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                PreferenceConnector.getPreferences(MyProfileActivity.this);
                PreferenceConnector.clearAll(MyProfileActivity.this);
                finish();
                Intent intent=new Intent(MyProfileActivity.this,GetstartedACtivity.class);
                startActivity(intent);
                finish();
                ActivityCompat.finishAffinity(MyProfileActivity.this);
                Utils.overidependingtrasition(MyProfileActivity.this);
            }
        })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alert11 = builder.create();
        alert11.show();

    }


    public void onBackClick(View view) {
        onBackPressed();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_PROFILE_REQUEST) {
            initview();
        }
    }
}
