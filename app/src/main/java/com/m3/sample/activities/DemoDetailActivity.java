package com.m3.sample.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.m3.sample.R;
import com.m3.sample.adapters.DemoNotificationAdapter;
import com.m3.sample.preferences.PreferenceConnector;

/**
 * Created by ${User} on 27/2/18.
 */

public class DemoDetailActivity   extends AppCompatActivity {
    ImageView mainimage;
    DemoNotificationAdapter adapter ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_detail);
        mainimage = (ImageView) findViewById(R.id.mainimage);
        final int product = getIntent().getIntExtra("product",0);
        if(product==1)
        {
            mainimage.setImageResource(R.mipmap.bahubali);
        }
        else
            mainimage.setImageResource(R.mipmap.business_2);
        ((Button) findViewById(R.id.cart)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(product==1)
                    PreferenceConnector.writeString("product_1","1",DemoDetailActivity.this);
                  else
                if(product==2)
                    PreferenceConnector.writeString("product_2","2",DemoDetailActivity.this);
                Toast.makeText(DemoDetailActivity.this,"Product added to cart!",Toast.LENGTH_SHORT).show();

                startActivity(new Intent(DemoDetailActivity.this,DemoHomeActivity.class));
                finish();
            }
        });
    }
    public void onBackClick(View v)
    {
        onBackPressed();
    }
}
