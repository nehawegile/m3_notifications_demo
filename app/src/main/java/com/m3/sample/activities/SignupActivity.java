package com.m3.sample.activities;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseOptions;
import com.google.gson.Gson;
import com.m3.sample.BuildConfig;
import com.m3.sample.R;
import com.m3.sample.Util.ConnectionCheck;
import com.m3.sample.Util.Utils;
import com.m3.sample.interfaces.APIInterface;
import com.m3.sample.preferences.PreferenceConnector;
import com.m3.sdk.DynamicPush;
import com.m3.sdk.callbacks.AuthorizationCallback;
import com.m3.sdk.models.DynamicPushEvent;
import com.m3.sample.webservice.APIClient;

import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {

    private ConnectionCheck mConnectionCheck;
    APIInterface apiInterface;
    String android_id ;
    @BindView(R.id.firstName)
    TextView firstName;
    @BindView(R.id.lastName)
    TextView lastName;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.password)
    TextView password;
    private HashMap<String,String> mUserProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        mUserProfile = new HashMap<>();

        android_id = PreferenceConnector.readString(PreferenceConnector.PREF_ANDROID_ID, "", this);
        if (mConnectionCheck == null) {
            ConnectionCheck.getInstance().setContext(getApplicationContext());
            mConnectionCheck = ConnectionCheck.getInstance();
        }

    }

    public void onclickback(View view){
        onBackPressed();
    }


    public void onclicksignin(View view){
        String fName=firstName.getText().toString().trim();
        String lName=lastName.getText().toString().trim();
        String passwrd=password.getText().toString().trim();
        Boolean isValidPassword=passwrd.contains(" ");
        String emailId=email.getText().toString();
        boolean isvalid = Utils.isValidEmail(emailId);
        if(isvalid == true){
            PreferenceConnector.writeString(PreferenceConnector.PREF_USER_EMAIL, emailId, this);
        }

        if (TextUtils.isEmpty(fName) || TextUtils.isEmpty(lName) ||  TextUtils.isEmpty(passwrd) || TextUtils.isEmpty(emailId) || isvalid==false ||isValidPassword==true ) {
            if (TextUtils.isEmpty(fName)) {
                Utils.callSnackbar(this, getString(R.string.please_enter_first_name));
            }
            else if ( TextUtils.isEmpty(lName) ) {
                Utils.callSnackbar(this, getString(R.string.please_enter_last_name));
            }
            else if (TextUtils.isEmpty(emailId)) {
                Utils.callSnackbar(this, getString(R.string.please_enter_email));
            }
            else if (isvalid==false){
                Utils.callSnackbar(this,  getString(R.string.please_enter_valid_email));
            }
            else if (TextUtils.isEmpty(passwrd) ) {
                Utils.callSnackbar(this, getString(R.string.please_enter_password));
            }
            else if(isValidPassword==true){
                Utils.callSnackbar(this, getString(R.string.password_not_contain_space));
            }
        } else{
         //   String name=fName + " " + lName;
            Utils.progressdialog(SignupActivity.this);

            PreferenceConnector.writeString(PreferenceConnector.PREF_USER_FIRST_NAME, fName, SignupActivity.this);
            PreferenceConnector.writeString(PreferenceConnector.PREF_USER_LAST_NAME, lName, SignupActivity.this);
            PreferenceConnector.writeString(PreferenceConnector.PREF_USER_PASSWORD, passwrd, SignupActivity.this);
            //gotoEnterPhoneNumberActivity();
            mUserProfile.put("name",fName);
            mUserProfile.put("email",emailId);
           //TODO Commnted
            //checkIsEmailExist();
//

            DynamicPush.getInstance(this).trackEvent(new DynamicPushEvent()
                    .withType("screen")
                    .withTarget("profile")
                    .withValue("opened"));

            saveUserProperties();
            authenticateApp();
        }
    }

    private void saveUserProperties() {
        if (mUserProfile.size()>0) {
            DynamicPush.getInstance(this).setUserProperties(mUserProfile);
        }
    }
    private void authenticateApp() {
        String apiKey = BuildConfig.apiKey;
        String apiSecretKey = BuildConfig.apiSecret;

//DEV

        FirebaseOptions firebaseOptions = new FirebaseOptions.Builder().setApiKey("AIzaSyBpkDZb8Z6bIeUYlUNUE7Rc1RE9UFL3cGA").setApplicationId("1:970235594675:android:7c4e4ca161caff8d").setGcmSenderId("970235594675").build();

        DynamicPush.initWith(this, apiKey, apiSecretKey, firebaseOptions, new AuthorizationCallback() {
            @Override
            public void onUserNotVerified() {
                Utils.dismissprogressdialog(SignupActivity.this);
                Toast.makeText(SignupActivity.this, "User is not verified !", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(@Nullable String reason) {
                Utils.dismissprogressdialog(SignupActivity.this);
                Toast.makeText(SignupActivity.this, "Error!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess() {
                Utils.dismissprogressdialog(SignupActivity.this);
                PreferenceConnector.writeBoolean(PreferenceConnector.PREF_IS_USER_LOGGEDIN, true, SignupActivity.this);

                Toast.makeText(SignupActivity.this, "Welcome to M3 !", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SignupActivity.this, DemoHomeActivity.class);
                intent.putExtra("email",mUserProfile.get("email"));
                intent.putExtra("name",mUserProfile.get("name"));
                PreferenceConnector.getPreferences(SignupActivity.this);
                PreferenceConnector.writeString("email",mUserProfile.get("email"),SignupActivity.this);
                PreferenceConnector.writeString("name",mUserProfile.get("name"),SignupActivity.this);

                startActivity(intent);
                Utils.overidependingtrasition(SignupActivity.this);
                ActivityCompat.finishAffinity(SignupActivity.this);
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}
