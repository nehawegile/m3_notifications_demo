package com.m3.sample.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;
import com.m3.sample.R;
import com.m3.sample.Util.ConnectionCheck;
import com.m3.sample.Util.Utils;
import com.m3.sample.interfaces.APIInterface;
import com.m3.sample.preferences.PreferenceConnector;
import com.m3.sample.webservice.APIClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsActivity extends AppCompatActivity {

    @BindView(R.id.changepassword)
    RelativeLayout changepassword;
    @BindView(R.id.changeemail)
    RelativeLayout changeemail;
    @BindView(R.id.pushtoggle)
    Switch pushtoggle;
    @BindView(R.id.yourlocation)
    TextView yourlocation;
    @BindView(R.id.email_password_layout)
    LinearLayout email_password_layout;
    APIInterface apiInterface;
    private ConnectionCheck mConnectionCheck;
    private BroadcastReceiver mMessageReceiver = null;
    String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("message")) {
                    message = intent.getStringExtra("message");
                    Utils.callSnackbar(SettingsActivity.this, message);
                }
            }
        };

        if (mConnectionCheck == null) {
            ConnectionCheck.getInstance().setContext(getApplicationContext());
            mConnectionCheck = ConnectionCheck.getInstance();
        }
        apiInterface = APIClient.getClient().create(APIInterface.class);

        String PREF_PUSH_NOTIFICATION = PreferenceConnector.readString(PreferenceConnector.PREF_PUSH_NOTIFICATION, "", SettingsActivity.this);
        if (PREF_PUSH_NOTIFICATION.equals("true")) {
            pushtoggle.setChecked(true);
        } else if (PREF_PUSH_NOTIFICATION.equals("false")) {
            pushtoggle.setChecked(false);
        }

        String PREF_CURRENT_LOCATION= PreferenceConnector.readString(PreferenceConnector.PREF_CURRENT_LOCATION, "", this);
        yourlocation.setText(PREF_CURRENT_LOCATION);

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("EVENT_SNACKBAR"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    public void onBackClick(View view) {
        onBackPressed();
    }

    @OnClick({R.id.changepassword, R.id.changeemail, R.id.tv_changepassword, R.id.tv_changeemail,
            R.id.ll_deleteaccount, R.id.deleteaccount, R.id.pushtoggle})
    public void changepassword(View view) {
        switch (view.getId()) {

            case R.id.ll_deleteaccount:
//                onCreateDialog();
                break;
            case R.id.deleteaccount:
//                onCreateDialog();
                break;
            case R.id.pushtoggle:
                if (pushtoggle.isChecked()) {
                    pushtoggle.setChecked(true);
                    PreferenceConnector.writeString(PreferenceConnector.PREF_PUSH_NOTIFICATION, "true", SettingsActivity.this);
                } else {
                    pushtoggle.setChecked(false);
                    PreferenceConnector.writeString(PreferenceConnector.PREF_PUSH_NOTIFICATION, "false", SettingsActivity.this);
                }
                break;
        }
    }


}
