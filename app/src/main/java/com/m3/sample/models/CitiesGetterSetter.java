package com.m3.sample.models;

/**
 * Created by Bharat on 12/7/16.
 */
public class CitiesGetterSetter {

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    String city;
}

