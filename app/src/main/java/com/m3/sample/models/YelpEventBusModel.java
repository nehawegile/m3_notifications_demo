package com.m3.sample.models;

/**
 * Created by wegile on 17/12/17.
 */

public class YelpEventBusModel {
    String id;

    public YelpEventBusModel(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
