package com.m3.sample.models;

/**
 * Created by wegile on 21/04/17.
 */
public class SelectCountryModelsList {


    String id;
    String country_name;

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String country_code;

}
