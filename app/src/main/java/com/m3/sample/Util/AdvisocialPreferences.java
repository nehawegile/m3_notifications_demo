package com.m3.sample.Util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by umang on 18/1/16.
 */
public class AdvisocialPreferences {



    static SharedPreferences pref;
    static AdvisocialPreferences advisocialPreferences;

    final String FREQUENCY = "FREQUENCY";
    final String SCHEDULAR_TIME = "SCHEDULAR_TIME";
    final String USER_ID = "USER_ID";
    final String CONTACT_ADD_DONE = "CONTACT_ADD_DONE";
    final String GPS_TRACKING = "GPS_TRACKING";
    public static String RANDOM_NUMBER = "random_number";

    final String IS_FIRST_TME = "IS_FIRST_TME";
    final String IS_PROFILE_SETUP = "IS_PROFILE_SETUP";

    final String REMAINING_SMS = "REMAINING_SMS";
    final String GPS_TRACKING_OCCURRENCE_INDEX = "GPS_TRACKING_OCCURRENCE_INDEX";
    final String USER_PHONE_NUMBER = "USER_PHONE_NUMBER";
    final String LOCATIONCHANGEINTERVAL = "LOCATIONCHANGEINTERVAL";
    final String OLDSERVERID = "OLDSERVERID";
    final String UPDATECLICKED = "UPDATECLICKED";
    final String ADDCONTACTSFIRST = "ADDCONTACTSFIRST";
    final String OPTIONALEMAIL = "OPTIONALEMAIL";
    final String USERNAME = "USERNAME";
    final String ISGPSTRACKONOFF = "ISGPSTRACKONOFF";
    final String ISCUSTOM_CHECK = "ISCUSTOM_CHECK";
    final String ISVERIFIED = "ISVERIFIED";
    final String DB_CHECK_IN_DATE = "DB_CHECK_IN_DATE";
    final String CHECK_IN_TIME = "CHECK_IN_TIME";
    final String CHECK_IN_DATE = "CHECK_IN_DATE";
    final String CHECK_IN_FREQUENCY= "CHECK_IN_FREQUENCY";
    final String SMS_AMOUNT= "SMS_AMOUNT";
    final String PRESENTSMSSTOREID="PRESENTSMSSTOREID";

    final String PASSCODE = "PASSCODE";

    final String ISPURCHASEAVAILABLE = "ISPURCHASEAVAILABLE";
    final String ISADDFREEPURCHASEAVAILABLE = "ISADDFREEPURCHASEAVAILABLE";
    final String ISADDFREEPURCHASED = "ISADDFREEPURCHASED";

    // For  GCM
    final String SEND_TOKEN_TO_SERVER = "SEND_TOKEN_TO_SERVER";
    final String SAVE_TOKEN = "SAVE_TOKEN";
    public static String ITEM_SELECTED = "item_selected";
    public static String COUNTRY_CODE = "COUNTRY_CODE";

    public static String C_NUMBER="C_NUMBER";

    public static AdvisocialPreferences getInstance(Context context){
        if (advisocialPreferences == null) {
            advisocialPreferences = new AdvisocialPreferences(context);
        }
        return advisocialPreferences;

    }

    public AdvisocialPreferences(Context context) {
        pref = context.getSharedPreferences("com.advisocial",
                Context.MODE_PRIVATE);
    }


    public  void setIsContactAddDone(boolean isContactAddDone) {
        pref.edit().putBoolean(CONTACT_ADD_DONE, isContactAddDone).commit();
    }

    public  boolean getIsContactAddDone() {
        return pref.getBoolean(CONTACT_ADD_DONE, false);
    }

    public int getSelectedCountry() {
        return pref.getInt(ITEM_SELECTED, -1);
    }

    public void setSelectedCountry(int selected) {
        pref.edit().putInt(ITEM_SELECTED, selected).commit();
    }


    public String getCnumber() {
        return pref.getString(C_NUMBER, "");
    }

    public void setCnumber(String selected) {
        pref.edit().putString(C_NUMBER, selected).commit();
    }

    public String getPRESENTSMSSTOREID() {
        return pref.getString(PRESENTSMSSTOREID, "");
    }

    public void setPRESENTSMSSTOREID(String selected) {
        pref.edit().putString(PRESENTSMSSTOREID, selected).commit();
    }

    public String getDB_CHECK_IN_DATE() {
        return pref.getString(DB_CHECK_IN_DATE, "");
    }

    public void setDB_CHECK_IN_DATE(String selected) {
        pref.edit().putString(DB_CHECK_IN_DATE, selected).commit();
    }

    public String getSMS_AMOUNT() {
        return pref.getString(SMS_AMOUNT, "");
    }

    public void setSMS_AMOUNT(String selected) {
        pref.edit().putString(SMS_AMOUNT, selected).commit();
    }

    public String getCHECK_IN_TIME() {
        return pref.getString(CHECK_IN_TIME, "Set Time");
    }

    public void setCHECK_IN_TIME(String selected) {
        pref.edit().putString(CHECK_IN_TIME, selected).commit();
    }

    public String getCHECK_IN_DATE() {
        return pref.getString(CHECK_IN_DATE, "Set Date");
    }

    public void setCHECK_IN_DATE(String selected) {
        pref.edit().putString(CHECK_IN_DATE, selected).commit();
    }
    public String getCHECK_IN_FREQUENCY() {
        return pref.getString(CHECK_IN_FREQUENCY, "Set Frequency");
    }

    public void setCHECK_IN_FREQUENCY(String selected) {
        pref.edit().putString(CHECK_IN_FREQUENCY, selected).commit();
    }



    public String getUSERNAME() {
        return pref.getString(USERNAME, "");
    }

    public void setUSERNAME(String selected) {
        pref.edit().putString(USERNAME, selected).commit();
    }
    public String getOPTIONALEMAIL() {
        return pref.getString(OPTIONALEMAIL, "Not Available");
    }

    public void setOPTIONALEMAIL(String selected) {
        pref.edit().putString(OPTIONALEMAIL, selected).commit();
    }

    public String getOLDSERVERID() {
        return pref.getString(OLDSERVERID, "");
    }

    public void setOLDSERVERID(String oldserverid) {
        pref.edit().putString(OLDSERVERID, oldserverid).commit();
    }


    public  void setFrequency(int frequencyIndex) {
        pref.edit().putInt(FREQUENCY, frequencyIndex).commit();
    }

    public  int getFrequency() {
        return pref.getInt(FREQUENCY, -1);
    }

    public  void setUserID(int userId) {
        pref.edit().putInt(USER_ID, userId).commit();
    }

    public  int getUserID() {
        return pref.getInt(USER_ID, -1);
    }


    public  void setGpsTracking(boolean status) {
        pref.edit().putBoolean(GPS_TRACKING, status).commit();
    }

    public  boolean getUPDATECLICKED() {
        return pref.getBoolean(UPDATECLICKED, false);
    }

    public  void setUPDATECLICKED(boolean updateclicked) {
        pref.edit().putBoolean(UPDATECLICKED, updateclicked).commit();
    }


    public  boolean getISVERIFIED() {
        return pref.getBoolean(ISVERIFIED, false);
    }

    public  void setISVERIFIED(boolean updateclicked) {
        pref.edit().putBoolean(ISVERIFIED, updateclicked).commit();
    }


    public  boolean getISPURCHASEAVAILABLE() {
        return pref.getBoolean(ISPURCHASEAVAILABLE, true);
    }

    public  void setISPURCHASEAVAILABLE(boolean ispurchaseavailable) {
        pref.edit().putBoolean(ISPURCHASEAVAILABLE, ispurchaseavailable).commit();
    }
    public  boolean getISADDFREEPURCHASEAVAILABLE() {
        return pref.getBoolean(ISADDFREEPURCHASEAVAILABLE, true);
    }

    public  void setISADDFREEPURCHASEAVAILABLE(boolean ispurchaseavailable) {
        pref.edit().putBoolean(ISADDFREEPURCHASEAVAILABLE, ispurchaseavailable).commit();
    }

    public  boolean getISADDFREEPURCHASED() {
        return pref.getBoolean(ISADDFREEPURCHASED, false);
    }

    public  void setISADDFREEPURCHASED(boolean ispurchaseavailable) {
        pref.edit().putBoolean(ISADDFREEPURCHASED, ispurchaseavailable).commit();
    }


    public  boolean getISGPSTRACKONOFF() {
        return pref.getBoolean(ISGPSTRACKONOFF, false);
    }

    public  void setISGPSTRACKONOFF(boolean updateclicked) {
        pref.edit().putBoolean(ISGPSTRACKONOFF, updateclicked).commit();
    }



    public  boolean getISCUSTOM_CHECKget() {
        return pref.getBoolean(ISCUSTOM_CHECK, false);
    }

    public  void setISCUSTOM_CHECK(boolean iscustom_check) {
        pref.edit().putBoolean(ISCUSTOM_CHECK, iscustom_check).commit();
    }
    public  boolean getAddcontactfirst() {
        return pref.getBoolean(ADDCONTACTSFIRST, true);
    }

    public  void setAddcontactfirst(boolean updateclicked) {
        pref.edit().putBoolean(ADDCONTACTSFIRST, updateclicked).commit();
    }

    public  boolean getGpsTraking() {
        return pref.getBoolean(GPS_TRACKING, false);
    }



    public int getRandomNumber() {
        return pref.getInt(RANDOM_NUMBER, -1);
    }

    public void setRandomNumber(int selected) {
        pref.edit().putInt(RANDOM_NUMBER, selected).commit();
    }



    public  void setIsFirstTime(boolean status) {
        pref.edit().putBoolean(IS_FIRST_TME, status).commit();
    }

    public  boolean getIsFirstTime() {
        return pref.getBoolean(IS_FIRST_TME, true);
    }


    public  void setProfilesetup(boolean status) {
        pref.edit().putBoolean(IS_PROFILE_SETUP, status).commit();
    }

    public  boolean getprofilesetup() {
        return pref.getBoolean(IS_PROFILE_SETUP, false);
    }



    public  void setGps_TrackingOccurrenceIndex(int userId) {
        pref.edit().putInt(GPS_TRACKING_OCCURRENCE_INDEX, userId).commit();
    }

    public  int getGps_TrackingOccurrenceIndex() {
        return pref.getInt(GPS_TRACKING_OCCURRENCE_INDEX, -1);
    }





    public  void setLOCATIONCHANGEINTERVAL(String locationchangeinterval) {
        pref.edit().putString(LOCATIONCHANGEINTERVAL, locationchangeinterval).commit();
    }

    public String getLOCATIONCHANGEINTERVAL() {
        return pref.getString(LOCATIONCHANGEINTERVAL, "Every 1 Hour");
    }



    public  void setCountryCode(String userId) {
        pref.edit().putString(COUNTRY_CODE, userId).commit();
    }

    public String getCountryCode() {
        return pref.getString(COUNTRY_CODE, "");
    }

    public  void setPASSCODE(String userId) {
        pref.edit().putString(PASSCODE, userId).commit();
    }

    public String getPASSCODE() {
        return pref.getString(PASSCODE, "");
    }



    public  void setUSER_PHONE_NUMBER(String user_phone_number) {
        pref.edit().putString(USER_PHONE_NUMBER, user_phone_number).commit();
    }

    public String getUSER_PHONE_NUMBER() {
        return pref.getString(USER_PHONE_NUMBER, "");
    }


    public  void setRemainingSMS(int userId) {
        pref.edit().putInt(REMAINING_SMS, userId).commit();
    }

    public  int getRemainingSMS() {
        return pref.getInt(REMAINING_SMS, 0);
    }

    public  void SetIsSendGCMToServer(boolean userId) {
        pref.edit().putBoolean(SEND_TOKEN_TO_SERVER, userId).commit();
    }

    public  boolean GetIsSendGCMToServer() {
        return pref.getBoolean(SEND_TOKEN_TO_SERVER, false);
    }

    public  void setSAVE_TOKEN(String userId) {
        pref.edit().putString(SAVE_TOKEN, userId).commit();
    }

    public String getSAVE_TOKEN() {
        return pref.getString(SAVE_TOKEN, "");
    }

    public void clearpreferences() {
       pref.edit().clear().commit();
    }


}
