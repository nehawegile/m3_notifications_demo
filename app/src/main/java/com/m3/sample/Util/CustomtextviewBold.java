package com.m3.sample.Util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.m3.sample.activities.M3ApplicationClass;

/**
 * Created by Bharat on 1/8/16.
 */
public class CustomtextviewBold extends TextView {


    public CustomtextviewBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomtextviewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomtextviewBold(Context context) {
        super(context);
        init();
    }

    public void init() {
        setTypeface(M3ApplicationClass.FontTypefaceArialRegularbold, 0);
        setClickable(true);
        // setTextColor(Color.WHITE);
    }
}
