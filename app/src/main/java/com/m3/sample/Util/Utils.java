package com.m3.sample.Util;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;


import com.m3.sample.R;
import com.m3.sample.activities.GetstartedACtivity;
import com.m3.sample.preferences.PreferenceConnector;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Bharat on 25/4/2017.
 */
public class Utils {

  /*  public static void startGpsService(Context context){

        Intent startServiceIntent = new Intent(context,LocationUpdate.class);
        PendingIntent startWebServicePendingIntent = PendingIntent.getBroadcast(context, 0, startServiceIntent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 10000, startWebServicePendingIntent);

    }*/

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 124;
    public static final int MY_PERMISSIONS_REQUEST_GPS = 125;
    public static boolean debug = true;

    private static ProgressDialog mProgressDialog;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermissionforgps(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("GPS permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_GPS);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_GPS);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public static void callSnackbar(Activity context, String message) {

//		Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
//		toast.setGravity(Gravity.TOP, 0, 0);
//		toast.show();
        Snackbar snackbar = Snackbar.make(context.findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(context.getResources().getColor(R.color.dark_gray2));
        snackbar.show();
    }

    public static boolean isinternetconnection(Context context) {
        ConnectivityManager connec = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        // ARE WE CONNECTED TO THE NET
        if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
            // MESSAGE TO SCREEN FOR TESTING (IF REQ)
            //Toast.makeText(this, connectionType + ” connected”, Toast.LENGTH_SHORT).show();
            return true;
        } else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
                || connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {
            return false;
        }

        return false;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkcameraPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.CAMERA)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Camera permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    /**
     * Print hash key
     */
    public static void printHashKey(Context context) {
        try {
            String TAG = "com.marketnyc";
            PackageInfo info = context.getPackageManager().getPackageInfo(TAG,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String keyHash = Base64.encodeToString(md.digest(),
                        Base64.DEFAULT);
//                if (Utils.debug)
                    Log.d(TAG, "keyHash: " + keyHash);
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException ignored) {

        }
    }


    public static String totalprice(String total){
        String totals[]=total.split("\\.");

        if(totals[1].equals("0")){
            total= totals[0];
        }
        return total;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void progressdialog(Context context) {

        try {
//            pd = ProgressDialog.show(context,title,message);
            if (mProgressDialog != null) {

                mProgressDialog.dismiss();
            }

        } catch (Exception e) {

        }

        try {
//            pd = ProgressDialog.show(context,title,message);
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.myprogress);

            ImageView image = (ImageView) mProgressDialog.findViewById(R.id.imageView_loader);
            Animation rotation = AnimationUtils.loadAnimation(context, R.anim.image_loader);
            rotation.setFillAfter(true);
            image.startAnimation(rotation);
            mProgressDialog.getWindow().setLayout(context.getResources().getDisplayMetrics().widthPixels,
                    context.getResources().getDisplayMetrics().heightPixels);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mProgressDialog.show();

        } catch (Exception e) {

        }
    }


    public static void overidependingtrasition(Activity activity) {
        activity.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }


    public static void dismissprogressdialog(Context context) {
        try {
//            pd = ProgressDialog.show(context,title,message);
            if (mProgressDialog != null) {

                mProgressDialog.dismiss();
            }

        } catch (Exception e) {

        }
    }


    public static JSONObject loadJSONobjectOfCountriesWithCountryName(Context context) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(loadCountriesWithCountryNameJSONFromAsset(context));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static String loadCountriesWithCountryNameJSONFromAsset(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("countriesWithCountyNameKey.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public static String getDaysDifferenceIST(long serverTimeLong) {
        // 2015-05-04 08:18:08

        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        Calendar c = Calendar.getInstance(TimeZone.getDefault());
        c.isSet(Calendar.AM_PM);
        Date Date1 = c.getTime();

        /*Date date2 = c.getTime();
        try {
            date2 = myFormat.parse(Date2);
        } catch (ParseException e) {
            e.printStackTrace();
            try {
                date2 = (new SimpleDateFormat("yyyy-MM-dd hh kk:mm:ss"))
                        .parse(Date2);
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        }*/


        long diff = 0;
        diff = serverTimeLong - Date1.getTime();

        int hours = (int) ((diff) / (1000 * 60 * 60));
        int min = (int) (diff - (1000 * 60 * 60 * hours)) / (1000 * 60);
        int seconds = (int) (diff - ((1000 * 60 * 60) * hours) - ((1000 * 60) * min)) / (1000);

       /* long daysdiff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        long Hoursdiff = TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS);
        long Mindiff = TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS);
        long Seconds = TimeUnit.SECONDS.convert(diff, TimeUnit.MILLISECONDS);*/

        return "" + hours + ":" + min + ":" + seconds;
    }


    public static String timeConvertion12_TO_24Format(String time12) {
        String time24Hour = "";
        //time = "12:15 a.m.";
        String timenew = time12.replace(".", "");

        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
            final Date dateObj = sdf.parse(timenew);
            System.out.println(dateObj);
            Log.e("time24...", String.valueOf(dateObj));
            SimpleDateFormat Hour24Format = new SimpleDateFormat("HH:mm");
            time24Hour = Hour24Format.format(dateObj);
            System.out.println(time24Hour);//
            Log.e("time24hours...", String.valueOf(time24Hour));

        } catch (final ParseException e) {
            e.printStackTrace();
        }
        return time24Hour;
    }

    // Match for valid email id
    public static boolean isValidEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String Convert24to12(String time) {
        String convertedTime = "";
        try {
            SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm aa");
            SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm");
            Date date = parseFormat.parse(time);
            convertedTime = displayFormat.format(date);
            System.out.println("convertedTime : " + convertedTime);
        } catch (final ParseException e) {
            e.printStackTrace();
        }
        return convertedTime;
        //Output will be 10:23 PM
    }


    public static String getLastSeen(long time) {

        SimpleDateFormat dateFormat;

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, 0);
        Calendar compare = Calendar.getInstance();
        compare.setTime(new Date(time));
        Calendar week = Calendar.getInstance();
        week.add(Calendar.DATE, -7);
        if (compare.before(week)) {
            dateFormat = new SimpleDateFormat("'Last seen' dd MMM 'at' hh:mm a");
        } else if (compare.before(calendar)) {
            dateFormat = new SimpleDateFormat("'Last seen' EEE 'at' hh:mm a");
        } else {
            dateFormat = new SimpleDateFormat("'Last seen' 'today' 'at' hh:mm a");
        }
        return dateFormat.format(compare.getTime());
    }

    public static long converttimetolongmillis(String time) {

        long lnttime = 0;

//        {"Every 1 Hour","Every 2 Hour", "Every 4 Hour", "Every 8 Hour", "Every 12 Hour", "Once a day"};

        if (time.equalsIgnoreCase("Every 1 Hour")) {

            lnttime = TimeUnit.HOURS.toMillis(1);

        }
        if (time.equalsIgnoreCase("Every 2 Hour")) {

            lnttime = TimeUnit.HOURS.toMillis(2);

        }
        if (time.equalsIgnoreCase("Every 4 Hour")) {

            lnttime = TimeUnit.HOURS.toMillis(4);

        }
        if (time.equalsIgnoreCase("Every 8 Hour")) {

            lnttime = TimeUnit.HOURS.toMillis(8);

        }
        if (time.equalsIgnoreCase("Every 12 Hour")) {

            lnttime = TimeUnit.HOURS.toMillis(12);

        }
        if (time.equalsIgnoreCase("Once a day")) {

            lnttime = TimeUnit.HOURS.toMillis(24);

        }

        return lnttime;
    }


    public static long convertTimeToLong(String srtTime) {
        long lngTime = 0;
        //  strTime format- 2014-01-26 05:36:38 PM
        srtTime = "2014-01-26 05:36 PM";

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
            Date now = formatter.parse(srtTime);
            lngTime = now.getTime();
            Log.i("Utils", "convertTimeToLong LOng time = " + lngTime);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return lngTime;
    }

    public static JSONObject loadJSONobjectOfCountriesWithCountryCode(Context context) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(loadCountriesWithCountryCodeJSONFromAsset(context));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }


    public static String loadCountriesWithCountryCodeJSONFromAsset(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("countriesWithCountyCodeKey.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getTimeZone() {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"),
                Locale.getDefault());
        Date currentLocalTime = calendar.getTime();
        DateFormat date = new SimpleDateFormat("Z");
        String localTime = date.format(currentLocalTime);
        Log.d("Time zone", "=" + localTime);

        String start = "" + localTime.subSequence(0, 3);
        String middle = ":";
        String end = "" + localTime.subSequence(3, localTime.length());
        localTime = start + middle + end;

        /*char ch = localTime.charAt(1);
        if(ch=='0'){
            String startNew=""+localTime.subSequence(0,1);
            Log.d("Time zone start -- ", "=" + startNew);
            String endNew = localTime.substring(2,localTime.length());
            Log.d("Time zone endNew -- ", "=" + endNew);
            localTime = startNew+endNew;
        }*/
        return localTime;
    }

    public static int getRandomNumber(int min, int max) {
        return (int) Math.floor(Math.random() * (max - min + 1)) + min;
    }


   public static void logOutDailog(final Context context){
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_dailog);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
       dialog.setCancelable(false);
        dialog.show();

        TextView ok = (TextView) dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceConnector.logout(context);
                Intent intent = new Intent(context, GetstartedACtivity.class);
                context.startActivity(intent);
                ActivityCompat.finishAffinity((Activity) context);
                Utils.overidependingtrasition((Activity) context);
                PreferenceConnector.logout(context);
            }
        });
    }

    public static void logOutDailog1(final Context context){
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_dailog);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();

        TextView ok = (TextView) dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceConnector.logout(context);
                Intent intent = new Intent(context, GetstartedACtivity.class);
                context.startActivity(intent);
                ActivityCompat.finishAffinity((Activity) context);
                Utils.overidependingtrasition((Activity) context);
                PreferenceConnector.logout(context);
            }
        });
    }

    public static void loginDailog(final Context context){
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.login_dailog);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
//        dialog.setCancelable(false);
        dialog.show();

        TextView ok = (TextView) dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceConnector.logout(context);
                Intent intent = new Intent(context, GetstartedACtivity.class);
                context.startActivity(intent);
                ActivityCompat.finishAffinity((Activity) context);
                Utils.overidependingtrasition((Activity) context);
                PreferenceConnector.logout(context);
            }
        });
    }

    public static String getTimeFormat(int hour, int min) {

        String strTime = "";
        String strHour = "", strMin = "", strAmPm = "";
        if (hour >= 12) {
            strAmPm = "pm";

        } else {
            strAmPm = "am";
        }

        if (hour < 10) {
            strHour = "0" + hour;
        } else {
            strHour = "" + hour;
        }

        if (min < 10) {
            strMin = "0" + min;
        } else {
            strMin = "" + min;
        }

        if (hour > 12) {
            hour = hour - 12;
        }

        strTime = strHour + " : " + strMin + " " + strAmPm;
        return strTime;
    }


    public static String addZero(int i) {
        if (i < 10) {

            return "0" + i;
        }
        return "" + i;
    }

    public static String getDateFromat(String dob) {
        int day=0; int month = 0;
        int year=0;
        String parts[]=dob.split("-");
        year= Integer.parseInt(parts[0]);
        day = Integer.parseInt(parts[2]);
        month = Integer.parseInt(parts[1]);
        String strFullDate = "";
        String strMonth = "";
        switch (month) {
            case 1:
                strMonth = "JAN";
                break;
            case 2:
                strMonth = "FEB";
                break;
            case 3:
                strMonth = "MAR";
                break;
            case 4:
                strMonth = "APR";
                break;
            case 5:
                strMonth = "MAY";
                break;
            case 6:
                strMonth = "JUN";
                break;
            case 7:
                strMonth = "JUL";
                break;
            case 8:
                strMonth = "AUG";
                break;
            case 9:
                strMonth = "SEP";
                break;
            case 10:
                strMonth = "OCT";
                break;
            case 11:
                strMonth = "NOV";
                break;
            case 12:
                strMonth = "DEC";
                break;
        }

        strFullDate = day + " " + strMonth + " " + year;

        return strFullDate;
    }

    public static String getFirstDateOfCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", new Locale("US"));
        c.set(Calendar.DATE, 1);
        return df.format(c.getTime());
    }

    public static String getFirstDateofMonth(Date mdate) {
        Calendar c = Calendar.getInstance();
        c.setTime(mdate);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        c.set(Calendar.DATE, 1);
        String formattedDate = df.format(c.getTime());
        System.out.println("formattedDate => " + formattedDate);

        return formattedDate;
    }

    public static Boolean isNetWorkingError(Context context) {
        try {
            ConnectivityManager ConMgr = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
//			if (ConMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
//					.isAvailable()
//					|| ConMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
//							.isAvailable())

            NetworkInfo netInfo = ConMgr.getActiveNetworkInfo();

            if (netInfo == null) {
//		            	 new AlertDialog.Builder(context)
//			                .setTitle(context.getResources().getString(R.string.app_name))
//			                .setMessage(Constants.INTERNET_CONNECTION_MESSAGE)
//			                .setPositiveButton("OK", null).show();
                Utils.ShowAlertDialogue(context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.app_name), context);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * This class used for checking that app is in foreground or not
     */
    public static class ForegroundCheckTask extends AsyncTask<Context, Void, Boolean> {

        private boolean isAppOnForeground(Context context) {
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
            if (appProcesses == null) {
                return false;
            }
            final String packageName = context.getPackageName();
            for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
                if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        protected Boolean doInBackground(Context... params) {
            final Context context = params[0].getApplicationContext();
            return isAppOnForeground(context);
        }
    }

    public static boolean isAppRunning(final Context context, final String packageName) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null)
        {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.processName.equals(packageName)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static Boolean isNetWorkingErrorcheckinactivity(Context context) {
        try {
            ConnectivityManager ConMgr = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
//			if (ConMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
//					.isAvailable()
//					|| ConMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
//							.isAvailable())

            NetworkInfo netInfo = ConMgr.getActiveNetworkInfo();

            if (netInfo == null) {
//		            	 new AlertDialog.Builder(context)
//			                .setTitle(context.getResources().getString(R.string.app_name))
//			                .setMessage(Constants.INTERNET_CONNECTION_MESSAGE)
//			                .setPositiveButton("OK", null).show();
//                Utils.ShowAlertDialogue(context.getResources().getString(R.string.app_name),context.getResources().getString(R.string.connection_failure), context);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static Boolean isNetWorkingErrormainscreen(Context context) {
        try {
            ConnectivityManager ConMgr = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
//			if (ConMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
//					.isAvailable()
//					|| ConMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
//							.isAvailable())

            NetworkInfo netInfo = ConMgr.getActiveNetworkInfo();

            if (netInfo == null) {
//		            	 new AlertDialog.Builder(context)
//			                .setTitle(context.getResources().getString(R.string.app_name))
//			                .setMessage(Constants.INTERNET_CONNECTION_MESSAGE)
//			                .setPositiveButton("OK", null).show();
                Utils.ShowAlertDialogue(context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.app_name), context);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void setopensansfont(TextView textView, Activity context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "fonts/OPENSANS-REGULAR.TTF");
        textView.setTypeface(tf);
    }

    public static void setopensansfontcontext(TextView textView, Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "fonts/OPENSANS-REGULAR.TTF");
        textView.setTypeface(tf);
    }

    public static void setopensansfontbold(TextView textView, Activity context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "fonts/OPENSANS-EXTRABOLD.TTF");
        textView.setTypeface(tf);
    }

    public static int convertDpToPixel(float dp, Context context) {

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);

    }

    public static int convertpxToDp(int px, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    static AlertDialog alertDialog = null;

    /*public static void ShowcustomAlertDialogue(String title,String message,Activity context){
*//*
        AlertDialog.Builder aleBuilder = new AlertDialog.Builder(context);

        aleBuilder.setTitle(title);
        aleBuilder.setMessage(message);
        aleBuilder.setCancelable(false);
        aleBuilder.setNeutralButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                alertDialog.dismiss();
            }
        });

        alertDialog = aleBuilder.create();
        try {
            alertDialog.show();
        } catch (Exception e) {
            // TODO: handle exception
        }*//*
        View view=View.inflate(context,R.layout.dialog,null);


        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        dialog.setCancelable(true);
        TextView titletext=(TextView)view.findViewById(R.id.titletextview);
        TextView messagetext=(TextView)view.findViewById(R.id.messagetextview);

     *//*   titletext.setText(title);
        messagetext.setText(message);*//*
        titletext.setText(title);
        messagetext.setText(message);

        dialog.show();
    }*/


    public static void ShowAlertDialogue(String title, String message, Context context) {

        AlertDialog.Builder aleBuilder = new AlertDialog.Builder(context);

        aleBuilder.setTitle(title);
        aleBuilder.setMessage(message);
        aleBuilder.setIcon(R.mipmap.ic_launcher);
        aleBuilder.setCancelable(false);
        aleBuilder.setNeutralButton(context.getString(R.string.app_name), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                alertDialog.dismiss();
            }
        });

        alertDialog = aleBuilder.create();
        try {
            alertDialog.show();
        } catch (Exception e) {
            // TODO: handle exception
        }

    }


    public static String getDeviceID(Context context) {
        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return android_id;
    }

}
