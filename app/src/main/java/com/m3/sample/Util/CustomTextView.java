package com.m3.sample.Util;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.m3.sample.activities.M3ApplicationClass;


/**
 * Created by wegile on 1/4/16.
 */
public class CustomTextView extends TextView {

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextView(Context context) {
        super(context);
        init();
    }

    public void init() {
        setTypeface(M3ApplicationClass.FontTypefaceArialRegular, 0);
        setClickable(true);
        // setTextColor(Color.WHITE);
    }
}
