package com.m3.sample.Util;

/**
 * Created by umang on 18/1/16.
 */
public class Constants {
    /*Constants for startActivity For Result*/

    public static final int PERMISSION_ALL = 2237;
    public static final int DESC_SRH_ACTIVITY = 1100;
    /**/
    public static final String ComfirmationFragmentKEY = "ComfirmationFragment";
    public static final String CreditCardPaymentFragmentKEY = "CreditCardPaymentFragment";
    public static final String PaymentMethodFragmentKEY = "PaymentMethodFragment";
    public static String PriceDetailsfragmentKEY = "PriceDetailsfragment";
    public static String WeekBookingTimingKEY = "WeekBookingTiming";
    public static String BookingDetailsKEY = "BookingDetails";

    public static final boolean DEBUG = true; // Debug logging

    public static final String DEVICE_USED = "Android";

    public static final String WEB_URL_STAGING = "http://52.28.94.50/api/";
    public static final String WEB_URL = "http://52.28.159.248/api/";
    public static final String WEB_SIGN_UP = "signup_api.php";  //
    public static final String WEB_SUCCESS_SIGN_UP = "success_signup_api.php";
    public static final String WEB_MANAGE_CONTACT = "manage_contacts_api.php";
    public static final String WEB_GET_CONTACT = "list_contacts_api.php";
    public static final String WEB_MANAGE_CHECK_IN_SCHEDULE = "manage_check_in_schedule_api.php";
    public static final String WEB_GET_USER_PROFILE = "get_user_profile.php";
    public static final String WEB_UPDATE_USER_PROFILE = "update_user_api.php";
    public static final String WEB_GET_SMS_PLAN = "sms_plan_data_api.php";
    public static final String WEB_GET_CUSTOM_CHECKIN = "get_custom_checkin_data_api.php";//
    public static final String WEB_CHECKIN = "checkin_api.php";
    public static final String WEB_MANAGE_CUSTOM_CHECKI_IN = "manage_custom_check_in_api.php";
    public static final String WEB_DELETE_CONTACT = "delete_contacts_api.php";
    public static final String WEB_DELETE_ACCOUNT = "del_account_api.php";
    public static final String INAPP_PURCHASE = "success_payment_api.php";

    public static final String INAPP_PURCHASEADDFREE = "update_ad_free_version.php";


    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 99;
    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTSAGAIN = 100;

    public static final int WEB_SIGN_UP_ID = 1;
    public static final int WEB_SUCCESS_SIGN_UP_ID = 2;
    public static final int WEB_MANAGE_CONTACT_ID = 3;
    public static final int WEB_MANAGE_CHECK_IN_SCHEDULE_ID = 4;
    public static final int WEB_GET_USER_PROFILE_ID = 5;
    public static final int WEB_UPDATE_USER_PROFILE_ID = 6;
    public static final int WEB_GET_SMS_PLAN_ID = 7;
    public static final int WEB_GET_CUSTOM_CHECKIN_ID = 8;
    public static final int WEB_SET_TOKEN_ID = 9;
    public static final int WEB_CHECKIN_ID = 10;
    public static final int WEB_MANAGE_CUSTOM_CHECKIN_ID = 11;
    public static final int DELETE_CUSTOM_CHECKIN_ID = 12;
    public static final int WEB_GET_PROFILE = 13;
    public static final int WEB_GET_CONTACT_ID = 14;
    public static final int WEB_GET_DELETE_CONTACT = 15;
    public static final int WEB_GET_DELETE_ACCOUNT = 16;
    public static final int WEB_LATLONG = 17;
    public static final int WEB_UPDATE_GPS = 18;
    public static final int WEB_IN_APP_PURCHASE = 19;
    public static final int WEB_IN_APP_PURCHASEADDFREE = 20;


    public static final String BACK_KEY_EVENT = "BACK_KEY_EVENT";


    public static int MIN_RANDUM_NUMBER = 100000;
    public static int MAX_RANDUM_NUMBER = 999999;


    public static final String UI_UPDATE = "UI_UPDATE";

    public static final String SERVER_ERROR = "server error";


    public static final int PICK_CONTACT = 123;
    public static final String[] COUNTRYLIST = {"United States", "Canada"};

    public static final String[] FREQUENCY_ARR = {"Every 4 hours", "Every 8 hours", "Every 12 hours", "Once a day", "Once in 2 days", "Once in 3 days", "Once a week"};
    public static final String[] FREQUENCY_ARR_SERVER = {"4", "8", "12", "24", "48", "72", "168"};
    public static final String[] HOUR_ARR = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
    public static final String[] MIN_ARR = {"00", "30"};
    public static final String[] AM_PM_ARR = {"AM", "PM"};

    public static final String[] GPS_ARR = {"Every 1 Hour", "Every 2 Hour", "Every 4 Hour", "Every 8 Hour", "Every 12 Hour", "Once a day"};
    public static final String[] GPS_ARR_SERVER = {"1", "2", "4", "8", "12", "24"};
    public static String COUNTRY_NAME = "country_name", COUNTRY_CODE = "country_code";




}
