package com.m3.sample.Util;


import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.m3.sample.activities.M3ApplicationClass;


/**
 * Created by wegile on 1/4/16.
 */
public class CustomEdittextRegular extends AppCompatEditText {

    public CustomEdittextRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomEdittextRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomEdittextRegular(Context context) {
        super(context);
        init();
    }

    public void init() {
        setTypeface(M3ApplicationClass.FontTypefaceArialRegular, 0);
        setClickable(true);
        // setTextColor(Color.WHITE);
    }
}
