package com.m3.sample.interfaces;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Created by wegile on 22/6/17.
 */

public interface APIInterface {
    //http://52.57.3.29:3000/api/signup

    @POST("/api/sign_up")
    Call<ResponseBody> signUp(@Body RequestBody userDetail);

    @POST("/api/login")
    Call<ResponseBody> signIn(@Body RequestBody detail);

    @POST("/api/loginFB")
    Call<ResponseBody> loginFB(@Body RequestBody detail);

    @POST("/user/forgotpass")
    Call<ResponseBody> forgotpass(@Body RequestBody detail);

    @POST("/api/validateEmailPhone")
    Call<ResponseBody> validateEmailPhone(@Body RequestBody detail);

    @POST("/user/changepass")
    Call<ResponseBody> changepass(@Body RequestBody detail);

    @POST("/user/get_business_schedule")
    Call<ResponseBody> get_business_schedule(@Body RequestBody detail);

    @GET("/user/products")
    Call<ResponseBody> products();

    @GET("/user/product_categories")
    Call<ResponseBody> product_categories();

    @POST("/user/all_business")
    Call<ResponseBody> all_business(@Query("page") String page, @Body RequestBody detail);

    @POST("/user/updateProfile")
    Call<ResponseBody> updateProfile(@Body RequestBody detail);

    @POST("user/getProfileInfo")
    Call<ResponseBody> getProfileInfo(@Body RequestBody detail);

    @POST("api/validateEmailFacebook")
    Call<ResponseBody> validateEmailFacebook(@Body RequestBody detail);

    @POST("user/getEmployees")
    Call<ResponseBody> getEmployees(@Body RequestBody detail);

    @POST("user/getSchedule")
    Call<ResponseBody> getSchedule(@Body RequestBody detail);

    @POST("user/new_booking")
    Call<ResponseBody> new_booking(@Body RequestBody detail);

    @POST("user/my_bookings")
    Call<ResponseBody> my_bookings(@Body RequestBody detail);

    @POST("user/cancel_booking")
    Call<ResponseBody> cancel_booking(@Body RequestBody detail);

    @Multipart
    @POST("/user/updateProfile")
    Call<ResponseBody> updateProfle(@PartMap Map<String, RequestBody> params, @Part MultipartBody.Part file);

    @POST("/user/sendFeedback")
    Call<ResponseBody> sendFeedback(@Body RequestBody detail);

    @POST("/user/add_favourite")
    Call<ResponseBody> add_favourite(@Body RequestBody detail);

    @POST("/user/remove_favourite")
    Call<ResponseBody> remove_favourite(@Body RequestBody detail);

    @POST("/user/favourite_list")
    Call<ResponseBody> favourite_list(@Body RequestBody detail);

    @POST("/user/compose_message")
    Call<ResponseBody> compose_message(@Body RequestBody detail);

    @POST("/user/inbox_list")
    Call<ResponseBody> inbox_list(@Body RequestBody detail);

    @POST("/user/sent_list")
    Call<ResponseBody> sent_list(@Body RequestBody detail);

    @POST("/user/updateDeviceToken")
    Call<ResponseBody> updateDeviceToken(@Body RequestBody detail);

    @POST("/user/place_order")
    Call<ResponseBody> place_order(@Body RequestBody detail);

    @POST("/user/order_history")
    Call<ResponseBody> order_history(@Body RequestBody detail);

    @POST("/user/product_review")
    Call<ResponseBody> product_review(@Body RequestBody detail);

    @POST("/user/add_to_cart")
    Call<ResponseBody> add_to_cart(@Body RequestBody detail);

    @POST("/user/remove_item")
    Call<ResponseBody> remove_item(@Body RequestBody detail);

    @POST("/user/get_cart")
    Call<ResponseBody> get_cart(@Body RequestBody detail);

    @POST("/user/calculate_shipping_price")
    Call<ResponseBody> calculate_shipping_price(@Body RequestBody detail);


    @POST("/user/conversation/{conversation_id}")
    Call<ResponseBody> conversation_id(@Path("conversation_id") String conversation_id, @Body RequestBody detail);

/*  @DELETE("user/client/{id}")
  Call<ResponseBody> deleteAccount(@Path("id") String itemId, @Body RequestBody detail);*/

    @HTTP(method = "DELETE", path = "user/client/{id}", hasBody = true)
    Call<ResponseBody> deleteAccount(@Path("id") String itemId, @Body RequestBody detail);

    @HTTP(method = "GET", path = "user/services/{business_id}")
    Call<ResponseBody> business_id(@Path("business_id") String itemId);

    @HTTP(method = "GET", path = "user/products/{business_id}")
    Call<ResponseBody> products(@Path("business_id") String itemId);

    @HTTP(method = "GET", path = "user/products_by_category/{category_id}")
    Call<ResponseBody> category_id(@Path("category_id") String itemId);

    @HTTP(method = "GET", path = "user/product_detail/{product_id}")
    Call<ResponseBody> product_detail(@Path("product_id") String itemId);

    @HTTP(method = "GET", path = "user/service_detail/{service_id}")
    Call<ResponseBody> service_detail(@Path("service_id") String itemId);

    @HTTP(method = "GET", path = "user/client_token")
    Call<ResponseBody> client_token();


//    @HTTP(method = "GET", path = "user/order_history/{client_id}")
//    Call<ResponseBody> order_history(@Path("client_id") String itemId);

 /* @POST("/api/users")
    Call<User> createUser(@Body User user);

    @GET("/api/users?")
    Call<UserList> doGetUserList(@Query("page") String page);*/

}
