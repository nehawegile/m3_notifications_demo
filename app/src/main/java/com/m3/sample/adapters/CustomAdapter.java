package com.m3.sample.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.m3.sample.R;


/**
 * Created by Hp on 16-08-2016.
 */
public class CustomAdapter<T> extends BaseAdapter {
    Context context;
    T[] array;
    LayoutInflater inflter;
    CustomClickCallBack customClickCallBack;

    public interface CustomClickCallBack {
        public void customClickCallback(String value);
    }

    public CustomAdapter(Context applicationContext, T[] array, Activity context) {
        this.context = applicationContext;
        this.array = array;
        inflter = (LayoutInflater.from(applicationContext));
        customClickCallBack= (CustomClickCallBack) context;
    }

    @Override
    public int getCount() {
        return array.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.listitemlayout, null, false);
        TextView text = (TextView) view.findViewById(R.id.spinnertext);
        text.setText(array[i].toString());
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customClickCallBack.customClickCallback(array[i].toString());
            }
        });
        return view;
    }
}