package com.m3.sample.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.m3.sample.R;
import com.m3.sample.models.SelectCountryModelsList;

import java.util.ArrayList;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

public class SelectCounryCodeAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private final ArrayList<SelectCountryModelsList> al_list_countries;
    private String[] countries;
    RelativeLayout select_country_layout;
    RelativeLayout header; LinearLayout below_header;

    private LayoutInflater inflater;
    TextView countryCode;

    public SelectCounryCodeAdapter(Context context, ArrayList<SelectCountryModelsList> al_list_countries, RelativeLayout select_country_layout,
                                   RelativeLayout header, LinearLayout below_header, TextView countryCode) {
        inflater = LayoutInflater.from(context);
        this.al_list_countries=al_list_countries;
        this.select_country_layout=select_country_layout;
        this.header=header;
        this.below_header=below_header;
        this.countryCode=countryCode;
    }

    @Override
    public int getCount() {
        return al_list_countries.size();
    }

    @Override
    public Object getItem(int position) {
        return al_list_countries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.select_country_adapter_layout, parent, false);
            holder.tv_countryname = (TextView) convertView.findViewById(R.id.tv_countryname);
            holder.tv_country_code = (TextView) convertView.findViewById(R.id.tv_country_code);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_countryname.setText(al_list_countries.get(position).getCountry_name());
        holder.tv_country_code.setText(al_list_countries.get(position).getCountry_code());

        holder.tv_countryname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slide_down(select_country_layout);
                select_country_layout.setVisibility(View.GONE);
                header.setVisibility(View.VISIBLE);
                below_header.setVisibility(View.VISIBLE);
                countryCode.setText(al_list_countries.get(position).getCountry_code());
            }
        });
        return convertView;
    }

    public void slide_down(RelativeLayout rLayout) {
        try {
            Animation slideDown = AnimationUtils.loadAnimation(inflater.getContext(),
                    R.anim.slide_down);
            rLayout.startAnimation(slideDown);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = inflater.inflate(R.layout.myheadercountrylist, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.text);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        //set header text as first char in name
        String headerText = "" + al_list_countries.get(position).getCountry_name().subSequence(0, 1).charAt(0);
        holder.text.setText(headerText);
        return convertView;
    }


    public long getHeaderId(int position) {
        //return the first character of the country as ID because this is what headers are based upon
        return al_list_countries.get(position).getCountry_name().subSequence(0, 1).charAt(0);
    }

    class HeaderViewHolder {
        TextView text;
    }

    class ViewHolder {
        TextView tv_countryname,tv_country_code;
    }

}

