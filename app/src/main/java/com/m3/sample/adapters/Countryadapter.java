package com.m3.sample.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.m3.sample.R;
import com.m3.sample.interfaces.Countryadapterlistener;
import com.m3.sample.models.CitiesGetterSetter;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Bharat on 12/7/16.
 */
public class Countryadapter extends BaseAdapter {


    private final Context context;
    private final ArrayList<CitiesGetterSetter> cityarraylist;
    private final Countryadapterlistener adapterlistener;
    private ViewHolder viewHolder;
    private LayoutInflater inflater;
    ArrayList<CitiesGetterSetter> array_l;

    public Countryadapter(Context context, ArrayList<CitiesGetterSetter> cityarraylist, Countryadapterlistener countryadapterlistener) {

        this.context=context;
        this.cityarraylist=cityarraylist;
        this.array_l=new ArrayList<CitiesGetterSetter>();
        this.array_l.addAll(cityarraylist);
        this.adapterlistener=countryadapterlistener;
    }

    @Override
    public int getCount() {
        return cityarraylist.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        if(convertView==null){
            viewHolder = new ViewHolder();
            inflater = (LayoutInflater) context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_item_cities, null);

            viewHolder.citytextview = (TextView)convertView.findViewById(R.id.citytextview);
            viewHolder.smallcitytextview = (TextView)convertView.findViewById(R.id.smalltextviewcity);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.citytextview.setText(cityarraylist.get(i).getCity());
        viewHolder.smallcitytextview.setText(cityarraylist.get(i).getCity());

        viewHolder.citytextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapterlistener.onchangestate(true,i);
            }
        });

        return convertView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        cityarraylist.clear();
        if (charText.length() == 0) {
            cityarraylist.addAll(array_l);
        } else {
            for (CitiesGetterSetter citiesGetterSetter : array_l) {
                if (citiesGetterSetter.getCity().toLowerCase(Locale.getDefault())
                        .startsWith(charText)) {
                    cityarraylist.add(citiesGetterSetter);
                }
            }
        }
        notifyDataSetChanged();
    }


    private class ViewHolder {

        TextView citytextview,smallcitytextview;
    }
}
