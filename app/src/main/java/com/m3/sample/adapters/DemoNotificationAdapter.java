package com.m3.sample.adapters;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.m3.sample.R;
import com.m3.sdk.models.PushNotification;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ${User} on 23/2/18.
 */

public class DemoNotificationAdapter extends RecyclerView.Adapter<DemoNotificationAdapter.CustomHolder>  {

    Context _context;
    ArrayList<PushNotification> productReviews;

    public DemoNotificationAdapter(Context context, ArrayList<PushNotification> productReviews) {
        this._context=context;
        this.productReviews=productReviews;
    }

    @Override
    public DemoNotificationAdapter.CustomHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.demo_notification_item, parent, false);
        DemoNotificationAdapter.CustomHolder vh = new DemoNotificationAdapter.CustomHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final DemoNotificationAdapter.CustomHolder holder, int position) {
        if(productReviews.get(position).getAnimation()!=null &&(productReviews.get(position).getAnimation().getUrl().contains(".mp3")
                || productReviews.get(position).getAnimation().getUrl().contains(".mp4")||productReviews.get(position).getAnimation().getUrl().contains(".3gp")))
        {
            holder.imageView.setVisibility(View.GONE);
            holder.videoView.setMediaController(new MediaController(_context));

            holder.videoView.setVideoPath(productReviews.get(position).getAnimation().getUrl());
//            holder.videoView.setVideoPath(String.valueOf(video));
            holder.videoView.setMediaController(null);

            holder.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
//                    holder.videoView.seekTo(1000);
                    holder.videoView.start();
                }
            });
        }
        else if(productReviews.get(position).getAnimation() !=null){
        Glide.with(_context).load(productReviews.get(position).getAnimation().getUrl()).asGif()
                .placeholder(R.drawable.progress_loader)
                .error(R.drawable.round_bg_black_trans)
                .into(holder.imageView);
            holder.videoView.setVisibility(View.GONE);
            holder.imageView.setVisibility(View.VISIBLE);

        }

        else {
            Glide.with(_context).load(R.mipmap.logo)
                    .placeholder(R.drawable.progress_loader)
                    .into(holder.imageView);
            holder.videoView.setVisibility(View.GONE);

        }
        holder.title.setText("Offer :");
        holder.mesg.setText(productReviews.get(position).getMessage());
    }

    @Override
    public int getItemCount() {
        return productReviews.size();
    }

    public class CustomHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img)
        ImageView imageView;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.mesg)
        TextView mesg;
        @BindView(R.id.video)
        VideoView videoView;
        public CustomHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}